FROM odoo:16.0

LABEL MAINTAINER Bryam Guzman <bguzman@ups.edu.ec>
USER root

COPY addons-saf/requirements.txt .
RUN pip3 install -r requirements.txt
# RUN pip3 install numpy
# RUN pip3 install beautifulsoup4

