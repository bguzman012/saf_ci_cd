from collections import defaultdict

from odoo import models, fields, api, _
from odoo.exceptions import AccessError, UserError, ValidationError
from odoo.tools import formatLang, float_compare, format_date, get_lang
import requests
import json


class MoveInherit(models.Model):
    _inherit = "account.move"
    sigac_empr_cdg = fields.Integer(
        string='Empresa',
        readonly=1)
    sigac_vchr_periodo = fields.Char(
        string='Periodo',
        readonly=1)
    sigac_tpvc_cdg = fields.Integer(
        string='Código TPVC',
        readonly=1)
    sigac_api_rest_code = fields.Integer(
        string='API REST código',
        readonly=1)
    sigac_vchr_crr_interno_mens = fields.Integer(
        string='Correlativo interno mensual',
        readonly=1)
    sigac_observaciones_ws = fields.Text(
        string='Observaciones',
        readonly=1)
    sigac_estado_ejecucion_ws = fields.Selection(
        selection=[
            ('EJEC', "Correcto"),
            ('ERR', "Error"),
            ('OTR', "Otro")
        ],
        string='Estado',
        default='OTR',
        help='Estado actual de la sincronización SIGAC-SAFAF')
    line_ids = fields.One2many(
        'account.move.line',
        'move_id',
        string='Journal Items',
        copy=True,
        readonly=True,
        states={'draft': [('readonly', False)]},
        index=True,
    )

    def _post(self, soft=True):
        """Post/Validate the documents.

        Posting the documents will give it a number, and check that the document is
        complete (some fields might not be required if not posted but are required
        otherwise).
        If the journal is locked with a hash table, it will be impossible to change
        some fields afterwards.

        :param soft (bool): if True, future documents are not immediately posted,
            but are set to be auto posted automatically at the set accounting date.
            Nothing will be performed on those documents before the accounting date.
        :return Model<account.move>: the documents that have been posted
        """
        if not self.env.su and not self.env.user.has_group('account.group_account_invoice'):
            raise AccessError(_("You don't have the access rights to post an invoice."))

        for invoice in self.filtered(lambda move: move.is_invoice(include_receipts=True)):
            if invoice.quick_edit_mode and invoice.quick_edit_total_amount and invoice.quick_edit_total_amount != invoice.amount_total:
                raise UserError(_(
                    "The current total is %s but the expected total is %s. In order to post the invoice/bill, "
                    "you can adjust its lines or the expected Total (tax inc.).",
                    formatLang(self.env, invoice.amount_total, currency_obj=invoice.currency_id),
                    formatLang(self.env, invoice.quick_edit_total_amount, currency_obj=invoice.currency_id),
                ))
            if invoice.partner_bank_id and not invoice.partner_bank_id.active:
                raise UserError(_(
                    "The recipient bank account linked to this invoice is archived.\n"
                    "So you cannot confirm the invoice."
                ))
            if float_compare(invoice.amount_total, 0.0, precision_rounding=invoice.currency_id.rounding) < 0:
                raise UserError(_(
                    "You cannot validate an invoice with a negative total amount. "
                    "You should create a credit note instead. "
                    "Use the action menu to transform it into a credit note or refund."
                ))

            if not invoice.partner_id:
                if invoice.is_sale_document():
                    raise UserError(
                        _("The field 'Customer' is required, please complete it to validate the Customer Invoice."))
                elif invoice.is_purchase_document():
                    raise UserError(
                        _("The field 'Vendor' is required, please complete it to validate the Vendor Bill."))

            # Handle case when the invoice_date is not set. In that case, the invoice_date is set at today and then,
            # lines are recomputed accordingly.
            if not invoice.invoice_date:
                if invoice.is_sale_document(include_receipts=True):
                    invoice.invoice_date = fields.Date.context_today(self)
                elif invoice.is_purchase_document(include_receipts=True):
                    raise UserError(_("The Bill/Refund date is required to validate this document."))

        if soft:
            future_moves = self.filtered(lambda move: move.date > fields.Date.context_today(self))
            for move in future_moves:
                if move.auto_post == 'no':
                    move.auto_post = 'at_date'
                msg = _('This move will be posted at the accounting date: %(date)s',
                        date=format_date(self.env, move.date))
                move.message_post(body=msg)
            to_post = self - future_moves
        else:
            to_post = self

        for move in to_post:
            if move.state == 'posted':
                raise UserError(_('The entry %s (id %s) is already posted.') % (move.name, move.id))
            if not move.line_ids.filtered(lambda line: line.display_type not in ('line_section', 'line_note')):
                raise UserError(_('You need to add a line before posting.'))
            if move.auto_post != 'no' and move.date > fields.Date.context_today(self):
                date_msg = move.date.strftime(get_lang(self.env).date_format)
                raise UserError(_("This move is configured to be auto-posted on %s", date_msg))
            if not move.journal_id.active:
                raise UserError(_(
                    "You cannot post an entry in an archived journal (%(journal)s)",
                    journal=move.journal_id.display_name,
                ))
            if move.display_inactive_currency_warning:
                raise UserError(_(
                    "You cannot validate a document with an inactive currency: %s",
                    move.currency_id.name
                ))

            affects_tax_report = move._affect_tax_report()
            lock_dates = move._get_violated_lock_dates(move.date, affects_tax_report)
            if lock_dates:
                move.date = move._get_accounting_date(move.invoice_date or move.date, affects_tax_report)

        # Create the analytic lines in batch is faster as it leads to less cache invalidation.
        to_post.line_ids._create_analytic_lines()
        to_post.write({
            'state': 'posted',
            'posted_before': True,
        })

        # Trigger copying for recurring invoices
        to_post.filtered(lambda m: m.auto_post not in ('no', 'at_date'))._copy_recurring_entries()

        for invoice in to_post:
            # Fix inconsistencies that may occure if the OCR has been editing the invoice at the same time of a user. We force the
            # partner on the lines to be the same as the one on the move, because that's the only one the user can see/edit.
            wrong_lines = invoice.is_invoice() and invoice.line_ids.filtered(lambda aml:
                                                                             aml.partner_id != invoice.commercial_partner_id
                                                                             and aml.display_type not in (
                                                                                 'line_note', 'line_section')
                                                                             )
            if wrong_lines:
                wrong_lines.write({'partner_id': invoice.commercial_partner_id.id})

            invoice.message_subscribe([
                p.id
                for p in [invoice.partner_id]
                if p not in invoice.sudo().message_partner_ids
            ])

            # Compute 'ref' for 'out_invoice'.
            if invoice.move_type == 'out_invoice' and not invoice.payment_reference:
                to_write = {
                    'payment_reference': invoice._get_invoice_computed_reference(),
                    'line_ids': []
                }
                for line in invoice.line_ids.filtered(
                        lambda line: line.account_id.account_type in ('asset_receivable', 'liability_payable')):
                    to_write['line_ids'].append((1, line.id, {'name': to_write['payment_reference']}))
                invoice.write(to_write)

            if (
                    invoice.is_sale_document()
                    and invoice.journal_id.sale_activity_type_id
                    and (invoice.journal_id.sale_activity_user_id or invoice.invoice_user_id).id not in (
                    self.env.ref('base.user_root').id, False)
            ):
                invoice.activity_schedule(
                    date_deadline=min((date for date in invoice.line_ids.mapped('date_maturity') if date),
                                      default=invoice.date),
                    activity_type_id=invoice.journal_id.sale_activity_type_id.id,
                    summary=invoice.journal_id.sale_activity_note,
                    user_id=invoice.journal_id.sale_activity_user_id.id or invoice.invoice_user_id.id,
                )

        customer_count, supplier_count = defaultdict(int), defaultdict(int)
        for invoice in to_post:
            if invoice.is_sale_document():
                customer_count[invoice.partner_id] += 1
            elif invoice.is_purchase_document():
                supplier_count[invoice.partner_id] += 1
        for partner, count in customer_count.items():
            (partner | partner.commercial_partner_id)._increase_rank('customer_rank', count)
        for partner, count in supplier_count.items():
            (partner | partner.commercial_partner_id)._increase_rank('supplier_rank', count)

        # Trigger action for paid invoices if amount is zero
        to_post.filtered(
            lambda m: m.is_invoice(include_receipts=True) and m.currency_id.is_zero(m.amount_total)
        )._invoice_paid_hook()

        if soft is False and to_post.journal_id.truc_cdg_sigac is not False:
            json_voucher = self.prepare_data_vouchers(to_post)
            res_ws = self.enviar_asiento_sigac(json_voucher)
            self.actualizar_info_asiento_sigac(res_ws, to_post, json_voucher)

        return to_post

    def enviar_asiento_sigac(self, json_voucher):
        endpoint = self.env['ir.config_parameter'].get_param('saf-activos-fijos.endpoint')
        asientos_url = self.env['ir.config_parameter'].get_param('saf-activos-fijos.endpoint_asientos')
        endpoint_asientos = endpoint + asientos_url
        try:
            reponse = requests.post(endpoint_asientos, json=json_voucher)
            return reponse
        except Exception:
            return False

    def prepare_data_vouchers(self, move):
        current_user = self.env.user
        detalle_vouchers = []
        number_line = 1
        for move_line in move.line_ids:
            tuple_centro_costo_dos = tuple(move_line.analytic_distribution.items())
            cuenta_analitica_dos = self.env['account.analytic.account']. \
                search([('id', '=', int(tuple_centro_costo_dos[0][0]))])

            if len(move_line.name) <= 44:
                cadena_referencia = move_line.name
            else:
                cadena_referencia = move_line.name[0:44]

            voucher_det = {
                # "cntsCdgCtable": "1110300155555555",
                "cntsCdgCtable": move_line.account_id.code,
                "dtvcDebe": move_line.debit,
                "dtvcFchCon": None,
                "dtvcGls": cadena_referencia,
                "dtvcHaber": move_line.credit,
                "dtvcNmrLinea": number_line,
                "esvcCdg": 99,
                "nctbCdgN1": move_line.centro_costo.codigo,
                "nctbCdgN2": cuenta_analitica_dos[0].code,
                "nctbCdgN3": None,
                "nctbCdgN4": None,
                "seqDtvcCdg": None,
                "tpanCdg": 0
            }
            detalle_vouchers.append(voucher_det)
            number_line += 1

        vch_mes = "0" + str(move.date.month) if len(str(move.date.month)) == 1 else str(move.date.month)
        vch_per = str(move.date.year) + '' + vch_mes
        truc_cdg_sigac = move.journal_id.truc_cdg_sigac
        json_voucher = {
            "detVouchers": detalle_vouchers,
            "emprCdg": 1,
            "esvcCdg": 99,
            "tpvcCdg": int(truc_cdg_sigac) if truc_cdg_sigac is not False else None,
            "usrsUsrName": current_user.usuario_sigac.upper(),
            "vchrCrrIntMen": 0 if move.sigac_vchr_crr_interno_mens is False else move.sigac_vchr_crr_interno_mens,
            "vchrDsc": move.ref + " " + move.date.strftime("%Y-%m-%dT%H:%M:%S"),
            "vchrFchAut": None,
            "vchrFchCom": move.date.strftime("%Y-%m-%dT%H:%M:%S"),
            "vchrPer": vch_per,
            "vchrTipGen": "A",
            "vchrTotal": move.amount_total
        }
        return json_voucher

    def actualizar_info_asiento_sigac(self, res_ws, move, json_voucher):

        # Si res_ws es nulo, quiere decir que el servicio esta caido
        if res_ws is False:
            asiento_tmp = {
                'sigac_empr_cdg': 1,
                'sigac_vchr_periodo': json_voucher['vchrPer'],
                'sigac_tpvc_cdg': move.journal_id.truc_cdg_sigac,
                'sigac_vchr_crr_interno_mens': None,
                'sigac_observaciones_ws': "Lo sentimos, el servicio presenta problemas o no se encuentra"
                                          " disponible en estos momentos, por favor"
                                          " inténtelo nuevamente en unos minutos",
                'sigac_estado_ejecucion_ws': 'ERR',
                'sigac_api_rest_code': 500
            }
        else:
            if res_ws.status_code == 404:
                raise ValidationError(
                    _('Error en la sincronización con el sistema SIGAC, el servicio no se encuentra disponible'))

            res_json = json.loads(res_ws.text)

            try:
                metadata = res_json['metadata'][0]
            except Exception:
                metadata = {'resultado': res_json['error'] if 'error' in res_json else 'Error desconocido'}

            try:
                voucher_res = res_json['voucherResponse']['vouchers'][0]
            except Exception:
                voucher_res = {'vchrCrrIntMen': None}
                pass

            asiento_tmp = {
                'sigac_empr_cdg': 1,
                'sigac_vchr_periodo': json_voucher['vchrPer'],
                'sigac_tpvc_cdg': move.journal_id.truc_cdg_sigac,
                'sigac_vchr_crr_interno_mens': voucher_res['vchrCrrIntMen'],
                'sigac_observaciones_ws': metadata['resultado'],
                'sigac_estado_ejecucion_ws': 'EJEC' if res_ws.status_code == 200 else 'ERR',
                'sigac_api_rest_code': res_ws.status_code
            }
        asiento_actualizado = move.write(asiento_tmp)
        return asiento_actualizado

