from odoo import models, fields, api


class MoveInherit(models.Model):
    _inherit = "account.journal"

    truc_cdg_sigac = fields.Char(string='Codigo de tipo de transacción')
