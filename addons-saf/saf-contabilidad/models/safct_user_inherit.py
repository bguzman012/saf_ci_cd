from odoo import models, fields, api


class UserInherit(models.Model):
    _inherit = "res.users"

    usuario_sigac = fields.Char(string='Usuario SIGAC')
    centros_costo = fields.Many2many('account.analytic.plan',
                                     'res_user_planes_rel',
                                     string='Centros de Costo',
                                     required=True,
                                     default=lambda self: self._centros_costo(),
                                     )
    centro_costo_predeterminado = fields.Many2one('account.analytic.plan',
                                                  string='Centros de Costo',
                                                  required=True,
                                                  default=lambda self: self._default_centro_costo(),
                                                  help='El centro de costo por defecto para las transacciones.'
                                                  )
    plan_depreciaciones = fields.Many2many('account.analytic.plan',
                                           'res_user_planes_rel_depreciaciones',
                                           string='Responsable(s) depreciaciones')

    def _centros_costo(self):
        centros_costo = self.env.user.centros_costo
        if centros_costo:
            return [centros_costo[0].id]

        plan = self.env['account.analytic.plan'].search([('company_id', '=', self.env.company.id)], limit=1)
        return [plan.id]

    def _default_centro_costo(self):
        centro_costo_usuario = self.env.user.centro_costo_predeterminado
        if centro_costo_usuario:
            return centro_costo_usuario[0]

        plan = self.env['account.analytic.plan'].search([('company_id', '=', self.env.company.id)], limit=1)
        return plan

    @api.onchange("login")
    def _compute_user(self):
        for user in self:
            try:
                user_sigac = user.login.split("@")[0]
            except:
                user_sigac = user.login
                pass
            user.usuario_sigac = user_sigac
