from collections import defaultdict

from odoo import models, fields, api, _
from odoo.exceptions import AccessError, UserError, ValidationError
from odoo.tools import formatLang, float_compare, format_date, get_lang
import requests
import json


class MoveInherit(models.Model):
    _inherit = "account.move.line"

    centro_costo = fields.Many2one('account.analytic.plan',
                                   string='Centro de Costo',
                                   domain=lambda self: self._get_allowed_centros_costo(),
                                   default=lambda self: self.env.user.centro_costo_predeterminado,
                                   help='El centro de costo por defecto para las transacciones.'
                                   )

    def _get_allowed_centros_costo(self):
        allowed_centros_costo = self.env.user.centros_costo.ids
        return [('id', 'in', allowed_centros_costo)]
