from odoo import models, fields, api


class AccountAnalyticAccountInherit(models.Model):
    _inherit = "account.analytic.account"

    plan_id = fields.Many2many(
        'account.analytic.plan',
        'saf_account_plan_rel',
        string='Plan',
        default=lambda self: self._centros_costo(),
    )
    root_plan_id = fields.Many2many(
        'account.analytic.plan',
        string='Root Plan',
        compute="_compute_root_plan",
        store=True,
    )

    @api.depends('plan_id', 'plan_id.parent_path')
    def _compute_root_plan(self):
        for account in self:
            account.root_plan_id = account.plan_id if account.plan_id else None

    def _centros_costo(self):
        all_plans = self.env['account.analytic.plan'].search([('company_id', '=', self.env.company.id)])
        return all_plans.ids

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('code', operator, name), ('name', operator, name)]
        return self._search(domain + args, limit=limit, access_rights_uid=name_get_uid)

    def name_get(self):
        result = []
        for rec in self:
            result.append((rec.id, '%s - %s' % (rec.code, rec.name)))
        return result


class AccountAnalyticLineInherit(models.Model):
    _inherit = "account.analytic.line"

    plan_id = fields.Many2many(
        'account.analytic.plan',
        store=True,
        readonly=True,
        compute_sudo=True,
    )


class AccountAnalyticPlanInherit(models.Model):
    _inherit = 'account.analytic.plan'

    users = fields.Many2many('res.users',
                             'res_user_planes_rel',
                             string='Usuario')
    account_ids = fields.Many2many(
        'account.analytic.account',
        'saf_account_plan_rel',
        string="Accounts",
        default=lambda self: self._cuentas_analiticas(),
    )
    codigo = fields.Char(
        string='Código',
        required=True,
        copy=False)
    users_depreciaciones = fields.Many2many('res.users',
                                            'res_user_planes_rel_depreciaciones',
                                            string='Responsable(s) depreciaciones')
    contacto = fields.Many2one('res.partner',
                               string="Contacto",
                               required=True)

    def _cuentas_analiticas(self):
        all__analityc_accounts = self.env['account.analytic.account']. \
            search([('company_id', '=', self.env.company.id)])
        return all__analityc_accounts.ids

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('codigo', operator, name), ('name', operator, name)]
        return self._search(domain + args, limit=limit, access_rights_uid=name_get_uid)

    def name_get(self):
        result = []
        for rec in self:
            result.append((rec.id, '%s - %s' % (rec.codigo, rec.name)))
        return result

    @api.model
    def get_relevant_plans(self, **kwargs):
        """ Returns the list of plans that should be available.
            This list is computed based on the applicabilities of root plans. """
        list_plans = []
        set_plan_ids = {}
        company_id = kwargs.get('company_id', self.env.company.id)
        centro_costo = self.env.user.centro_costo_predeterminado

        # Pendiente agregar filtro para el centro de costo uno del usuario
        all_plans = self.search([('company_id', '=', company_id), ('id', '=', centro_costo.id)], limit=1)
        for plan in all_plans:
            applicability = plan._get_applicability(**kwargs)
            if applicability != 'unavailable':
                set_plan_ids[plan.id] = plan
                list_plans.append(
                    {
                        "id": plan.id,
                        "name": plan.name,
                        "color": plan.color,
                        "applicability": applicability,
                        "all_account_count": plan.all_account_count
                    })
        # If we have accounts that are already selected (before the applicability rules changed or from a model),
        # we want the plans that were unavailable to be shown in the list (and in optional, because the previous
        # percentage could be different from 0)
        # record_account_ids = kwargs.get('existing_account_ids', [])
        # forced_plans = self.env['account.analytic.account'].browse(record_account_ids).mapped('root_plan_id')
        # for plan in forced_plans.filtered(lambda plan: plan.id not in set_plan_ids):
        #     list_plans.append({
        #         "id": plan.id,
        #         "name": plan.name,
        #         "color": plan.color,
        #         "applicability": 'optional',
        #         "all_account_count": plan.all_account_count
        #     })
        return sorted(list_plans, key=lambda d: (d['applicability'], d['id']))
