from odoo import models, fields, api
import traceback


class SAFContabilidadInherit(models.Model):
    _inherit = "account.account"

    tipos_codigos_contab = {
        '1': 'asset_current',
        '2': 'liability_current',
        '3': 'equity',
        '4': 'income',
        '5': 'expense',
        '7': 'off_balance',
        '8': 'off_balance'
    }

    @api.model
    def create_cuenta(self, vals):
        try:
            cuenta_json = vals

            cuenta_save = {
                'name': cuenta_json['name'],
                'code': cuenta_json['code'],
                'account_type': 'asset_cash' if cuenta_json['es_banco'] == 'S' else self.tipos_codigos_contab[
                    cuenta_json['code'][0][0]],
                'company_id': self.env.company.id
            }

            cuenta = self.env['account.account'].create(cuenta_save)

            return {'RESULTADO':
                        {'Descripcion': "Registro guardado exitosamente",
                         "Id": cuenta.id}}
        except Exception:
            return {'RESULTADO': 'Error ' + traceback.format_exc()}

    @api.model
    def actualizar_cuenta(self, vals):
        try:
            cuenta_json = vals

            cuenta = self.env['account.account'].sudo(). \
                search([('code', '=', cuenta_json['code']),
                        ('deprecated', '=', False)])

            if cuenta.id is False:
                return {'RESULTADO': 'Error, cuenta contable no existe'}

            cuenta_json = {
                'name': cuenta_json['name'],
                'account_type': 'asset_cash' if cuenta_json['es_banco'] == 'S' else self.tipos_codigos_contab[
                    cuenta_json['code'][0][0]]
            }
            cuenta.write(cuenta_json)
            return {'RESULTADO':
                        {'Descripcion': "Registro actualizado exitosamente",
                         "Id": cuenta.id}}
        except Exception:
            return {'RESULTADO': 'Error ' + traceback.format_exc()}

    @api.model
    def eliminar_cuenta(self, vals):
        try:
            cuenta_json = vals

            cuenta = self.env['account.account'].sudo().search([('code', '=', cuenta_json['code'])])
            if cuenta.id is False:
                return {'RESULTADO': 'Error, cuenta contable no existe'}

            cuenta_json = {
                'deprecated': True
            }
            cuenta.write(cuenta_json)
            return {'RESULTADO':
                        {'Descripcion': "Registro eliminado exitosamente",
                         "Id": cuenta.id}}
        except Exception:
            return {'RESULTADO': 'Error ' + traceback.format_exc()}

