from odoo import models, fields, api


class HrEmployeeInherit(models.Model):
    _inherit = "hr.employee"

    centro_costo = fields.Many2one('account.analytic.plan',
                                   string='Centro de Costo',
                                   required=True,
                                   default=lambda self: self._default_centro_costo(),
                                   help='Centro de costo al que pertenece el empleado.'
                                   )

    def _default_centro_costo(self):
        centro_costo_usuario = self.env.user.centro_costo_predeterminado
        if centro_costo_usuario:
            return centro_costo_usuario[0]

        plan = self.env['account.analytic.plan'].search([('company_id', '=', self.env.company.id)], limit=1)
        return plan
