# -*- coding: utf-8 -*-

from . import safct_user_inherit
from . import safct_move_inherit
from . import safct_acc_journal_inherit
from . import safct_analytic_account_inherit
from . import safct_move_line_inherit
from . import safct_hr_employee_inherit
from . import safct_inherit
