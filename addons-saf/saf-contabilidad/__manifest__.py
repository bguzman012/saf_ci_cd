# -*- coding: utf-8 -*-
{
    'name': "Contabilidad UPS",
    'summary': """
        Modulo de contabilidad UPS""",
    'description': """
        Módulo de contabilidad UPS
    """,
    'author': "UPS",
    'website': "https://www.ups.edu.ec/",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': ['base', 'account', 'mail'],
    'data': [
        'views/user_inherit.xml',
        'views/account_journal_inherit.xml',
        'views/account_move_inherit.xml',
        'views/account_move_line_inherit.xml',
        'views/analytic_account_views_inherit.xml',
        'views/hr_employee_inherit.xml',
        'views/analytic_account_plan_views_inherit.xml'
    ],
    'assets': {
        'web.assets_backend': [
            'saf-contabilidad/static/src/components/**/*',
        ]
    },
}
