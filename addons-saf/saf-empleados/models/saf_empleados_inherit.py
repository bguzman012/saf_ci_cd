from odoo import models, fields, api
import traceback


class SAFHrEmployeeInherit(models.Model):
    _inherit = "hr.employee"

    codigo_persona = fields.Char(string='Codigo de persona GTH', readonly=True)

    gth_estado_civil = {
        1: 'married',
        2: 'divorced',
        3: 'single',
        4: 'widower',
        5: 'cohabitant'
    }

    @api.model
    def create_empleado(self, vals):
        try:
            obj_centro_costo = self.env['account.analytic.plan'].search([
                ('codigo', '=', vals['centro_costo'])])

            if obj_centro_costo.id is False:
                return {'RESULTADO': 'Error, no existe centro de costo'}

            if 'tipo_identificacion_codigo' in vals:
                if vals['tipo_identificacion_codigo'] == 1:
                    tipo_ident = 1
                else:
                    tipo_ident = 2
            else:
                tipo_ident = None

            partner_save = {
                'name': vals['nombres'] + ' ' + vals['apellidos'],
                'street': vals['calle_uno'] if 'calle_uno' in vals else None,
                'street2': vals['calle_dos'] if 'calle_dos' in vals else None,
                'l10n_latam_identification_type_id': tipo_ident,
                'vat': vals['num_identificacion'] if 'num_identificacion' in vals else None
            }

            contacto = self.env['res.partner'].create(partner_save)

            empleado_save = {
                'name': vals['nombres'] + ' ' + vals['apellidos'],
                'codigo_persona': vals['per_codigo'],
                'centro_costo': obj_centro_costo.id,
                'work_contact_id': contacto.id,
                'marital': self.gth_estado_civil[
                    vals['estado_civil_codigo']] if 'estado_civil_codigo' in vals else None
            }

            empleado = self.env['hr.employee'].create(empleado_save)
            return {'RESULTADO':
                        {'Descripcion': "Registro guardado exitosamente",
                         "Id": empleado.id}}
        except Exception:
            return {'RESULTADO': 'Error ' + traceback.format_exc()}

    @api.model
    def actualizar_empleado(self, vals):
        try:
            empleado_obj = self.env['hr.employee'].search([
                ('codigo_persona', '=', vals['per_codigo'])])

            if empleado_obj.id is False:
                return {'RESULTADO': 'Error, no existe empleado'}

            contact_obj = empleado_obj.work_contact_id[0]

            obj_centro_costo = self.env['account.analytic.plan'].search([
                ('codigo', '=', vals['centro_costo'])])

            if obj_centro_costo.id is False:
                return {'RESULTADO': 'Error, no existe centro de costo'}

            if 'tipo_identificacion_codigo' in vals:
                if vals['tipo_identificacion_codigo'] == 1:
                    tipo_ident = 1
                else:
                    tipo_ident = 2
            else:
                tipo_ident = None

            partner_save = {
                'name': vals['nombres'] + ' ' + vals['apellidos'],
                'street': vals['calle_uno'] if 'calle_uno' in vals else None,
                'street2': vals['calle_dos'] if 'calle_dos' in vals else None,
                'l10n_latam_identification_type_id': tipo_ident,
                'vat': vals['num_identificacion'] if 'num_identificacion' in vals else None
            }

            contact_obj.write(partner_save)

            empleado_save = {
                'name': vals['nombres'] + ' ' + vals['apellidos'],
                'centro_costo': obj_centro_costo.id,
                'marital': self.gth_estado_civil[
                    vals['estado_civil_codigo']] if 'estado_civil_codigo' in vals else None
            }

            empleado_obj.write(empleado_save)
            return {'RESULTADO':
                        {'Descripcion': "Registro actualizado exitosamente",
                         "Id": empleado_obj.id}}
        except Exception:
            return {'RESULTADO': 'Error ' + traceback.format_exc()}

    @api.model
    def eliminar_empleado(self, vals):
        try:
            empleado_obj = self.env['hr.employee'].search([
                ('codigo_persona', '=', vals['per_codigo'])])

            if empleado_obj.id is False:
                return {'RESULTADO': 'Error, no existe empleado'}

            contact_obj = empleado_obj.work_contact_id[0]
            contact_obj.write({'active': False})
            empleado_obj.write({'active': False})
            return {'RESULTADO':
                        {'Descripcion': "Registro eliminado exitosamente",
                         "Id": empleado_obj.id}}
        except Exception:
            return {'RESULTADO': 'Error ' + traceback.format_exc()}
