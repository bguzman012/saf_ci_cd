# -*- coding: utf-8 -*-
{
    'name': "SAF Empleados",
    'summary': """
        Modulo de empleados UPS""",
    'description': """
        Modulo de empleados UPS
    """,
    'author': "UPS",
    'website': "https://www.ups.edu.ec/",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': ['base', 'account', 'mail'],
    'data': [
        'views/saf_empleados_inherit.xml',
    ]
}
