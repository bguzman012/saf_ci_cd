# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, AccessError, ValidationError
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
from odoo.fields import float_round


class ActivoFijo(models.Model):
    _name = 'saf.activo_fijo'
    _description = 'Activo fijo'
    _rec_name = 'searching'
    _order = 'write_date desc'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Text(
        string='Descripción',
        required=True)
    bnaf_resumen = fields.Text(
        string='Descripción')
    codigo_sigac = fields.Integer(
        string='SIGAC código',
        readonly=True,
        index=True,
        copy=False, )
    codigo_barras_sigac = fields.Char(
        string='SIGAC código barras',
        readonly=True,
        copy=False,
        default="Codigo de barras anterior SIGAC")
    active = fields.Boolean(
        'Active',
        default=True)
    state = fields.Selection(
        selection=[
            ('borr', "Borrador"),
            ('val', "Validado"),
            ('baj', "Baja"),
            ('control', "Bien Control")
        ],
        string='Estado',
        default='borr',
        tracking=True,
        help='Estado actual del activo')
    acf_codigo_activo = fields.Char(
        string='Código Activo',
        required=True,
        readonly=True,
        copy=False,
        default="Codigo TMP")
    searching = fields.Char(
        string='Searching Activo',
        copy=False,
        required=True)
    taf_codigo = fields.Many2one(
        string='Tipo Activo Fijo',
        comodel_name='saf.tipo_activo_fijo',
        required=True)
    acf_fecha_alta = fields.Date(
        string='Fecha Alta',
        required=True,
        default=fields.Date.today())
    acf_fecha_inicio_depreciacion = fields.Date(
        string='Fecha Inicio Depreciación',
        required=True,
        help='Es necesario tener permisos necesarios (habilitado_cambio_inicio_depreciacion)'
             ' para modificar el inicio de depreciación',
        default=fields.Date.today())
    acf_valor_alta = fields.Monetary(
        string='Valor Alta',
        required=True)
    acf_valor_reposicion = fields.Monetary(
        string='Valor Reposición',
        required=True)
    acf_valor_revalorizacion = fields.Float(
        string='Valor Revalorización',
        digits=(16, 6))
    acf_vida_util_restante = fields.Integer(
        string='Vida Útil Inicial',
        required=True,
        help="La vida útil debe ser ingresada en numero de meses")

    acf_vida_util_residual = fields.Integer(
        string='Vida Útil Restante',
        required=True,
        help="Vida útil restante del activo fijo")

    tiene_permisos_cambiar_vu = fields.Boolean(
        compute='_permisos_vida_util',
        string='Tiene permisos para el cambio de vida útil?.'
    )
    tiene_permisos_cambiar_init_dep = fields.Boolean(
        compute='_permisos_init_dep',
        string='Tiene permisos para el cambio del inicio de depreciación?.'
    )
    acf_factura_compra = fields.Char(
        string='Num. Factura')
    acf_fecha_compra = fields.Date(
        string='Fecha',
        default=fields.Date.today())
    acf_valor_baja = fields.Float(
        string='Valor Baja',
        digits=(16, 6))
    # dependencia = fields.Many2one(
    #     string='Dependencia',
    #     comodel_name='saf.centro_costo',
    #     required=True, readonly=True)
    afid = fields.Many2one(
        comodel_name='saf.asistente_activo_fijo',
        string="Asistente Activo Fijo",
        ondelete='cascade')
    baf_codigo = fields.Many2many(
        string='Baja Activo Fijo',
        comodel_name='saf.baja_activo_fijo',
        required=True)
    acf_fecha_baja = fields.Datetime(
        string='Fecha Baja')
    custodio = fields.Many2one(
        comodel_name='hr.employee',
        string="Custodio",
        ondelete='cascade',
        required=True)
    proveedor = fields.Many2one(
        comodel_name='res.partner',
        string="Proveedor")
    caracteristicas = fields.One2many(
        comodel_name='saf.caracteristica_activo_fijo',
        inverse_name='acf_codigo',
        string='Caracteristicas de Activo Fijo',
        index=True)
    depreciacion = fields.One2many(
        comodel_name='saf.depeciacion_activo_fijo',
        inverse_name='acf_codigo',
        index=True,
        string='Depreciación de activo')
    transferencia = fields.Many2many(
        comodel_name='saf.transferencias',
        string='Transferencias',
        required=True)
    company_id = fields.Many2one(
        "res.company",
        string="Company",
        readonly=True,
        default=lambda self: self.env.company,
    )
    centro_costo_uno = fields.Many2one(
        "account.analytic.plan",
        string="Centro de Costo 1",
        domain=lambda self: self._get_allowed_centros_costo(),
        default=lambda self: self.env.user.centro_costo_predeterminado,
        required=True
    )
    centro_costo_dos = fields.Many2one(
        "account.analytic.account",
        string="Centro de Costo 2"
    )
    currency_id = fields.Many2one(
        "res.currency",
        string="Moneda",
        default=2
    )
    total_transferencias = fields.Integer(
        compute='_transf_total',
        string="Total Transferencias")

    num_depreciaciones_realizadas = fields.Integer(
        compute='_depreciaciones_total_realizadas',
        string="Total Depreciaciones")
    print_format = fields.Selection([
        ('dymo', 'Dymo'),
        ('2x7xprice', '2 x 7 with price'),
        ('4x7xprice', '4 x 7 with price'),
        ('4x12', '4 x 12'),
        ('4x12xprice', '4 x 12 with price')], string="Format", default='dymo', required=True)

    @api.onchange('centro_costo_uno')
    def _onchange_centro_costo_uno(self):
        self.custodio = None

    def _get_allowed_centros_costo(self):
        allowed_centros_costo = self.env.user.centros_costo.ids
        return [('id', 'in', allowed_centros_costo)]

    @api.constrains('acf_valor_alta')
    def _check_unidad_ing(self):
        for record in self:
            if record.acf_valor_alta <= 0:
                raise ValidationError("El valor de alta no puede ser menor o igual a 0")

    def _depreciaciones_total_realizadas(self):
        for rec in self:
            lista_depreciaciones_realizadas = self.env['saf.depeciacion_activo_fijo'].search(
                [('acf_codigo', '=', rec.id), ('daf_status', '=', 'DEP')])
            rec.num_depreciaciones_realizadas = len(lista_depreciaciones_realizadas)

    def _permisos_vida_util(self):
        for rec in self:
            if self.env.user.has_group('saf-activos-fijos.habilitado_vida_util_cambio'):
                rec.tiene_permisos_cambiar_vu = True
            else:
                rec.tiene_permisos_cambiar_vu = False

    def _permisos_init_dep(self):
        for rec in self:
            if self.env.user.has_group('saf-activos-fijos.habilitado_cambio_inicio_depreciacion'):
                rec.tiene_permisos_cambiar_init_dep = True
            else:
                rec.tiene_permisos_cambiar_init_dep = False

    def button_draft(self):
        self.write({'state': 'borr'})

    def _transf_total(self):
        for rec in self:
            total_transferencias = self.env['saf.transferencias'].search(
                [('tra_activos_fijos', '=', rec.id), ('state', '=', 'val')])
            rec.total_transferencias = len(total_transferencias)

    def action_transferencia_id(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("saf-activos-fijos.action_transferencias")
        ids_trans = [trans.id for trans in self.transferencia]
        action['domain'] = [
            ('id', 'in', ids_trans),
            ('state', '=', 'val')
        ]
        return action

    @api.onchange('acf_fecha_alta')
    def _onchange_fecha_alta(self):
        for record in self:
            record.acf_fecha_inicio_depreciacion = record.acf_fecha_alta
            if self.env.user.has_group('saf-activos-fijos.habilitado_cambio_inicio_depreciacion'):
                record.tiene_permisos_cambiar_init_dep = True
            else:
                record.tiene_permisos_cambiar_init_dep = False

    @api.onchange('taf_codigo')
    def _onchange_taf(self):
        for record in self:
            record.acf_vida_util_restante = record.taf_codigo.taf_vida_util
            if self.env.user.has_group('saf-activos-fijos.habilitado_vida_util_cambio'):
                record.tiene_permisos_cambiar_vu = True
            else:
                record.tiene_permisos_cambiar_vu = False

    def method_name(self):
        view_id = self.env.ref('saf-activos-fijos.view_trasnferencias_tree').id
        context = self._context.copy()
        return {
            'view_mode': 'form',
            'res_model': 'saf.activo_fijo',
            'view_id': view_id,
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'target': 'new',
            'context': context
        }

    def create_af_migracion(self, val):
        activo_creado = super(ActivoFijo, self).create(val)
        return activo_creado

    @api.model
    def create(self, val):
        if "afid" not in dict.keys(val):

            # Revisa si tiene permisos para cambiar vida util
            if not self.env.user.has_group('saf-activos-fijos.habilitado_vida_util_cambio'):
                vida_util_taf = self.env['saf.tipo_activo_fijo'].search(
                    [('id', '=', val['taf_codigo'])])[0]['taf_vida_util']
                val['acf_vida_util_restante'] = vida_util_taf

            self.state = 'borr'
            codigo_activo = "Borrador"
            cadena_searching = val['name'] + ' [' + str(codigo_activo) + ']'
            vals = {
                'centro_costo_uno': val['centro_costo_uno'],
                'centro_costo_dos': val['centro_costo_dos'],
                'name': val['name'],
                'searching': cadena_searching,
                'acf_codigo_activo': codigo_activo,
                'taf_codigo': val['taf_codigo'],
                'acf_fecha_alta': val['acf_fecha_alta'],
                'acf_fecha_inicio_depreciacion': val['acf_fecha_inicio_depreciacion'],
                'acf_valor_alta': val['acf_valor_alta'],
                'acf_vida_util_restante': val['acf_vida_util_restante'],
                'acf_vida_util_residual': val['acf_vida_util_restante'],
                'proveedor': val['proveedor'],
                'acf_valor_reposicion': val['acf_valor_reposicion'],
                'acf_valor_revalorizacion': val['acf_valor_revalorizacion'] if "acf_valor_revalorizacion" in dict.keys(
                    val) else None,
                'custodio': val['custodio'] if "custodio" in dict.keys(val) else None,
                'acf_factura_compra': val['acf_factura_compra'] if "acf_factura_compra" in dict.keys(
                    val) else None,
                'acf_fecha_compra': val['acf_fecha_compra'] if "acf_fecha_compra" in dict.keys(
                    val) else None
            }
            activo_creado = super(ActivoFijo, self).create(vals)

            self.calcular_tabla_depreciacion(activo_creado)
        else:
            activo_creado = super(ActivoFijo, self).create(val)
        return activo_creado

    def write_depreciacion(self, val):
        activo_actualizado = super(ActivoFijo, self).write(val)
        return activo_actualizado

    def write(self, val):
        if "afid" not in dict.keys(val):
            if 'taf_codigo' not in val:
                val['taf_codigo'] = self.taf_codigo.id
            if 'acf_vida_util_restante' not in val:
                val['acf_vida_util_restante'] = self['acf_vida_util_restante']

            if self['tiene_permisos_cambiar_vu'] is False:
                vida_util_taf = self.env['saf.tipo_activo_fijo'].search(
                    [('id', '=', val['taf_codigo'])])[0]['taf_vida_util']
                vida_util_restante = vida_util_taf
            else:
                vida_util_restante = val['acf_vida_util_restante']

            if self['acf_vida_util_restante'] == vida_util_restante:
                val['acf_vida_util_restante'] = self['acf_vida_util_restante']
            else:
                val['acf_vida_util_restante'] = vida_util_restante

            if self.num_depreciaciones_realizadas == 0:
                val['acf_vida_util_residual'] = val['acf_vida_util_restante']

            # es_depreciacion = False
            # if "depreciacion" not in dict.keys(val):
            # val['acf_vida_util_residual'] = val['acf_vida_util_restante']
            # else:
            #     del val["depreciacion"]
            #     es_depreciacion = True

            activo_actualizado = super(ActivoFijo, self).write(val)

            # and es_depreciacion is False
            if self.num_depreciaciones_realizadas == 0:
                lista_depreciaciones = self.env['saf.depeciacion_activo_fijo'].search(
                    [('acf_codigo', '=', self.id)])
                lista_depreciaciones.unlink()

                self.calcular_tabla_depreciacion(self)
        else:
            activo_actualizado = super(ActivoFijo, self).write(val)

        return activo_actualizado

    def validate(self):

        # Verifica si se encuentra en los dias permitidos para realizar los movimientos
        ir_default = self.env['ir.default'].sudo()
        dia_cierre = ir_default.get('res.config.settings', 'fecha_corte_operaciones')

        if int(dia_cierre) != 0:
            fecha_hoy = date.today()
            dia_fin_mes = self.last_day_of_month(fecha_hoy).strftime('%d')
            if int(dia_cierre) > int(dia_fin_mes):
                dia_cierre = dia_fin_mes
            fecha_actual = datetime.now()
            anio_mes_actual = fecha_actual.strftime('%Y-%m')
            fecha_actual_corte = anio_mes_actual + '-' + str(dia_cierre)
            fecha_actual_corte_date = datetime.strptime(fecha_actual_corte, '%Y-%m-%d').date()
            if fecha_hoy >= fecha_actual_corte_date:
                mensaje_error = 'No es posible realizar esta operaciÓn, se encuentra fuera de la fecha ' \
                                'limite para validar movimientos: ' + str(fecha_actual_corte_date)
                raise AccessError(
                    mensaje_error)
            #

        self.state = 'val'
        self.active = True
        json_seq_codes = {'111': 'saf-activos-fijos.seq_barcode_rec',
                          '112': 'saf-activos-fijos.seq_barcode_cue',
                          '113': 'saf-activos-fijos.seq_barcode_uio',
                          '114': 'saf-activos-fijos.seq_barcode_gye'}
        codigo_centro_costo = self.centro_costo_uno.codigo
        num_seq = json_seq_codes[codigo_centro_costo]
        seq_num_af = self.env['ir.sequence'].next_by_code(num_seq)
        code_taf = self.taf_codigo.taf_codigo_contable

        while len(code_taf) < 3: code_taf = '0' + code_taf

        codigo_af = str(codigo_centro_costo) + str(code_taf) + seq_num_af
        self.acf_codigo_activo = codigo_af
        self.searching = self.name + ' [' + str(codigo_af) + ']'

        if self.num_depreciaciones_realizadas == 0:
            self.acf_vida_util_residual = self.acf_vida_util_restante
        return

    def calcular_tabla_depreciacion(self, activo_creado):
        moneda = self.env.company.currency_id.id
        moneda_precision_decimal = self.env.company.currency_id.decimal_places

        nperiodos = activo_creado.acf_vida_util_restante
        v_inicial = activo_creado.acf_valor_alta

        v_dep_final = 0
        v_util_residual = nperiodos
        v_util_ini_anio = nperiodos
        v_dep_acumulada = 0
        v_dep_acumulada_final = 0
        v_dep_mes = 0

        if activo_creado.acf_fecha_inicio_depreciacion.day > 14:
            # Si el dia es mayor a 14, se crea la primera depreciacion el siguiente mes
            fecha_inicio_deps = activo_creado.acf_fecha_inicio_depreciacion
            if fecha_inicio_deps.day > 28:
                fecha_inicio_deps = fecha_inicio_deps.replace(day=28)
            carry, new_month = divmod(fecha_inicio_deps.month - 1 + 1, 12)
            new_month += 1
            fecha_inicio_dep = fecha_inicio_deps.replace(year=fecha_inicio_deps.year + carry, month=new_month)
            finmes = self.last_day_of_month(fecha_inicio_dep)
        else:
            finmes = self.last_day_of_month(activo_creado.acf_fecha_inicio_depreciacion)

        for periodo in range(nperiodos):
            if finmes.month == 1:
                v_dep_acumulada = 0
                v_dep_final = v_dep_acumulada_final
                v_util_ini_anio = v_util_residual
            else:
                v_dep_acumulada += v_dep_mes

            v_util_residual -= 1
            v_meses = v_util_ini_anio - v_util_residual
            dif_val_init_dep_fin = v_inicial - v_dep_final
            div_val_init_dep_fin_res_ini = dif_val_init_dep_fin / v_util_ini_anio
            mul_val_init_dep_fin_res_ini_meses = div_val_init_dep_fin_res_ini * v_meses
            v_dep_mes = mul_val_init_dep_fin_res_ini_meses - v_dep_acumulada
            v_dep_acumulada_final += v_dep_mes
            vals = {
                'acf_codigo': activo_creado.id,
                'mon_codigo': moneda,
                'daf_periodo_depreciacion': finmes,
                'daf_vida_util_residual': v_util_residual,
                'daf_valor_inicial_activo': v_inicial,
                'daf_depreciacion_mes': float_round(v_dep_mes, precision_digits=moneda_precision_decimal),
                'daf_depreciacion_acum': float_round(v_dep_acumulada_final, precision_digits=moneda_precision_decimal),
                'daf_valor_residual': float_round(v_inicial - v_dep_acumulada_final,
                                                  precision_digits=moneda_precision_decimal),
                'daf_status': 'PEN'
            }

            self.env['saf.depeciacion_activo_fijo'].create(vals)
            finmes = self.last_day_of_month(finmes + timedelta(days=1))
        return

    def last_day_of_month(self, date):
        if date.month == 12:
            return date.replace(day=31)
        return date.replace(month=date.month + 1, day=1) - timedelta(days=1)

    def unlink(self):
        for obj_af in self:
            if obj_af.num_depreciaciones_realizadas > 0:
                raise AccessError(
                    'No es posible eliminar activos que se han depreciado al menos una vez con respecto a su vida '
                    'útil')
            if obj_af.state == 'val':
                raise AccessError(
                    'No es posible eliminar activos que ya se encuentran en estado Validado')
            return models.Model.unlink(self)

    def generar_codigo_barras(self):
        self.ensure_one()
        xml_id, data = self._prepare_report_data()
        report_action = self.env.ref(xml_id).report_action(self)
        report_action.update({'close_on_report_download': True})
        return report_action

    def _prepare_report_data(self):

        xml_id = 'saf-activos-fijos.report_barcode_af'

        active_model = 'saf.activo_fijo'

        # Build data to pass to the report
        data = {
            'active_model': active_model,
            'quantity_by_product': {1},
            'layout_wizard': self.id,
            'price_included': 'xprice' in self.print_format,
        }
        return xml_id, data

    def generar_codigo_barras_tree(self):
        lista_activos = []
        for activo in self:
            if activo.state == 'borr':
                continue

            json_af = {
                'centro_costo': activo.centro_costo_uno.name,
                'name': activo.name,
                'codigo_barras': activo.acf_codigo_activo
            }
            lista_activos.append(json_af)

        data = {
            'lista_activos': lista_activos,
            'form': self.read()[0]
        }
        xml_id = 'saf-activos-fijos.report_barcode_tree_af_masive'
        report_action = self.env.ref(xml_id).report_action(self, data=data)
        report_action.update({'close_on_report_download': True})
        return report_action
