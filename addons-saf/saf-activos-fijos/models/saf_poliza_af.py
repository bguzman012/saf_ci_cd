# -*- coding: utf-8 -*-

from odoo import models, fields


class PolizaActivoFijo(models.Model):
    _name = 'saf.poliza_activo_fijo'
    _description = 'Poliza Activo Fijo'
    acf_codigo = fields.Many2one(
        string='Activo Fijo',
        comodel_name='saf.activo_fijo',
        required=True)
    tip_codigo = fields.Many2one(
        string='Tipo Poliza',
        comodel_name='saf.tipo_poliza',
        required=True)
    paf_fecha_vencimiento = fields.Datetime(
        string='Fecha Vencimiento',
        required=True)

