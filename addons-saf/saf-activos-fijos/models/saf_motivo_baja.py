# -*- coding: utf-8 -*-

from odoo import models, fields


class MotivoBaja(models.Model):
    _name = 'saf.motivos_baja'
    _description = 'Motivos Baja Activo Fijo'
    name = fields.Text(
        string='Descripción',
        required=True)
    code = fields.Text(
        string='Código',
        required=True)

    _sql_constraints = [('name_baja_motivo_unique',
                         'unique(name)',
                         'La descripcion ingresada debe ser única')
                        ]
