# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class TipoActivoFijo(models.Model):
    _name = 'saf.tipo_activo_fijo'
    _description = 'Tipo Activo Fijo'
    _rec_name = 'complete_name'
    _order = 'name'

    taf_codigo_padre = fields.Many2one(
        string='Activo Padre',
        comodel_name='saf.tipo_activo_fijo')
    complete_name = fields.Char(
        'Complete Name',
        compute='_compute_complete_name',
        recursive=True,
        store=True)
    name = fields.Char(
        string='Descripcion',
        required=True)
    taf_abreviatura = fields.Char(
        string='Abreviatura')
    taf_codigo_contable = fields.Char(
        string='Codigo Contable')
    taf_depreciable = fields.Boolean(
        'Depreciable',
        default=False)
    taf_vida_util = fields.Integer(
        string='Vida Util',
        required=True)
    lista_car_codigo = fields.One2many(
        string='Cuentas Contables',
        comodel_name='saf.tipo_activo',
        inverse_name='taf_codigo',
        ondelete='cascade')
    _sql_constraints = [('name_tipo_activo_unique',
                         'unique(name)',
                         'La descripcion ingresada debe ser única'),
                        ('abr_tipo_activo_unique',
                         'unique(taf_abreviatura)',
                         'La abreviatura ingresada debe ser única')
                        ]

    @api.depends('name', 'taf_codigo_padre.complete_name')
    def _compute_complete_name(self):
        for object in self:
            if object.taf_codigo_padre:
                object.complete_name = '%s / %s' % (object.taf_codigo_padre.complete_name, object.name)
            else:
                object.complete_name = object.name

    def name_get(self):
        result = []
        for rec in self:
            result.append((rec.id, '%s' % rec.name))

        return result

    @api.model
    def default_get(self, fields):
        res = super(TipoActivoFijo, self).default_get(fields)
        if fields[0] == 'taf_codigo_padre':
            cuentas_lines = [(0, 0, {'tac_tipo_cuenta': 'ACTIVO'}), (0, 0, {'tac_tipo_cuenta': 'BAJA'}),
                             (0, 0, {'tac_tipo_cuenta': 'DEPRECIACION'}),
                             (0, 0, {'tac_tipo_cuenta': 'DEPRECIACION ACUMULADA'})]
            res.update({'lista_car_codigo': cuentas_lines})
            return res
        else:
            return res

    @api.model
    def create(self, vals):
        if vals['taf_vida_util'] == 0:
            raise ValidationError('La vida util no puede tener valor 0')

        tipo_af = super(TipoActivoFijo, self).create(vals)
        return tipo_af
