# -*- coding: utf-8 -*-

from odoo import models, fields


class CaracteristicaTipoActivo(models.Model):
    _name = 'saf.caracteristica_tipo_activo'
    _description = 'Caracteristica Tipo Activo'
    _order = 'cta_orden asc'

    taf_codigo = fields.Many2one(
        string='Tipo de Activo Fijo',
        comodel_name='saf.tipo_activo_fijo',
        required=True)
    car_codigo = fields.Many2one(
        string='Caracteristica',
        comodel_name='saf.caracteristica',
        ondelete='cascade',
        required=True)
    cta_obligatorio = fields.Selection(
        [('S', 'Si'), ('N', 'No')],
        string='Obligatorio',
        required=True)
    cta_orden = fields.Integer(
        string='Orden  de Visualizacion')
