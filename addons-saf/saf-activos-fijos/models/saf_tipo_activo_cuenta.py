# -*- coding: utf-8 -*-

from odoo import models, fields


class TipoActivoCuenta(models.Model):
    _name = 'saf.tipo_activo'
    _description = 'Tipo Activo Cuenta'
    taf_codigo = fields.Many2one(
        string='Tipo Activo Fijo',
        comodel_name='saf.tipo_activo_fijo',
        required=True,
        ondelete='cascade')
    cuc_codigo = fields.Many2one(
        string='Codigo Cuenta Contable',
        comodel_name='account.account')
    tac_tipo_cuenta = fields.Char(
        string='Tipo Cuenta')
    company_id = fields.Many2one(
        "res.company",
        string="Company",
        default=lambda self: self.env.company,
    )

