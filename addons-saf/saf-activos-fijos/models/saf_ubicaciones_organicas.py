# -*- coding: utf-8 -*-

from odoo import models, fields, api


class UbicacionesOrganicas(models.Model):
    _name = 'saf.ubicaciones_organicas'
    _parent_store = True

    name = fields.Char(string='Codigo',
                       required=True
                       )
    code = fields.Char(string='Nombre',
                       required=True)
    parent_path = fields.Char(index=True, unaccent=False)
    parent_id = fields.Many2one('saf.ubicaciones_organicas', 'Ubicacion Padre', ondelete='restrict')
    parent_left = fields.Integer('Parent Left', index=True)
    parent_right = fields.Integer('Parent  Right', index=True)

    child_ids = fields.One2many('saf.ubicaciones_organicas', 'parent_id', 'Ubicaciones Hijos')

    _sql_constraints = [('code_ubi_org_unique',
                         'unique(name)',
                         'El codigo ingresado debe ser unico')
                        ]

    def name_get(self):
        result = []
        for rec in self:
            result.append((rec.id, '%s - %s' % (rec.code, rec.name)))
        return result

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('code', operator, name), ('name', operator, name)]
        return self._search(domain + args, limit=limit, access_rights_uid=name_get_uid)
