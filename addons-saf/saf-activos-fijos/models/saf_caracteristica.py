# -*- coding: utf-8 -*-
from odoo import models, fields, api


class Caracteristica(models.Model):
    _name = 'saf.caracteristica'
    _description = 'Caracteristica'

    name = fields.Char(
        string='Descripcion',
        required=True)
    car_tipo = fields.Selection(
        [('S', 'Selección'), ('T', 'Texto'), ('N', 'Numero'), ('F', 'Fecha')],
        string='Tipo',
        required=True,
        default="S")
    lista_car_codigo = fields.One2many(
        string='Lista de caracteristicas',
        comodel_name='saf.lista_caracteristica',
        inverse_name='car_codigo')
    caracteristica_tipo_activo = fields.One2many(
        comodel_name='saf.caracteristica_tipo_activo',
        inverse_name='car_codigo',
        string='Tipos de activos fijos',
        help='Tipos de activo a los que ha sido asignada esta caracteristica')
