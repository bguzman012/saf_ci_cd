# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError, AccessError
from datetime import datetime, timedelta, date


class TransferenciaGenerador(models.Model):
    _name = 'saf.transferencias'
    _description = 'Transferencias'
    _order = 'tra_fecha_movimiento desc'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(
        copy=False,
        string='Código',
        default="Nuevo",
        readonly=True)
    tra_activos_fijos = fields.Many2many(
        comodel_name='saf.activo_fijo',
        string='Activos Fijos',
        required=True)
    tra_custodio_origen = fields.Many2one(
        comodel_name='hr.employee',
        string="Custodio Origen",
        ondelete='cascade',
        required=True)
    tra_custodio_destino = fields.Many2one(
        comodel_name='hr.employee',
        string="Custodio Destino",
        ondelete='cascade',
        required=True)
    tra_observaciones = fields.Text(
        string='Observaciones')
    tra_fecha_movimiento = fields.Date(
        string='Fecha Movimiento',
        required=True,
        default=fields.Date.today())
    tra_custodio_origen_id = fields.Integer(
        string='Custodio Origen Id',
        compute='_compute_custodio_origen',
        store=False)
    tra_lugar_origen = fields.Char(
        string='Lugar Origen',
        compute='_compute_custodio_origen',
        store=False)
    tra_lugar_destino = fields.Char(
        string='Lugar Destino',
        compute='_compute_custodio_destino',
        store=False)
    state = fields.Selection(
        selection=[
            ('borr', "Borrador"),
            ('val', "Validado")
        ],
        string='Estado',
        default='borr',
        tracking=True,
        help='Estado actual del activo')
    company_id = fields.Many2one(
        "res.company",
        string="Company",
        readonly=True,
        default=lambda self: self.env.company,
    )
    sede_origen = fields.Many2one(
        "account.analytic.plan",
        string="Sede Origen",
        domain=lambda self: self._get_allowed_centros_costo(),
        default=lambda self: self.env.user.centro_costo_predeterminado,
        required=True
    )
    sede_destino = fields.Many2one(
        "account.analytic.plan",
        string="Sede Origen",
        domain=lambda self: self._get_allowed_centros_costo(),
        default=lambda self: self.env.user.centro_costo_predeterminado,
        required=True
    )

    def init(self):

        # Necesario para que tome el formato
        self.env.company.write({
            'paperformat_id': self.env.ref('saf-plantillas-reportes.format_a4_ups').id,
        })
        sup = super(TransferenciaGenerador, self)
        if hasattr(sup, 'init'):
            sup.init()

    def _get_allowed_centros_costo(self):
        allowed_centros_costo = self.env.user.centros_costo.ids
        return [('id', 'in', allowed_centros_costo)]

    @api.depends('tra_custodio_destino')
    def _compute_custodio_destino(self):
        for rec in self:
            rec.tra_lugar_destino = rec.tra_custodio_destino.department_id.name

    @api.depends('tra_custodio_origen')
    def _compute_custodio_origen(self):
        for rec in self:
            rec.tra_custodio_origen_id = rec.tra_custodio_origen.id
            rec.tra_lugar_origen = rec.tra_custodio_origen.department_id.name

    @api.model
    def create(self, val):
        if len(val['tra_activos_fijos'][0][2]) == 0:
            raise ValidationError(_('Por favor seleccione al menos un activo fijo...'))

        generador_transferencia = super(TransferenciaGenerador, self).create(val)
        return generador_transferencia

    def validar_transferencias(self):

        # Verifica si se encuentra en los dias permitidos para realizar los movimientos
        ir_default = self.env['ir.default'].sudo()
        dia_cierre = ir_default.get('res.config.settings', 'fecha_corte_operaciones')

        if int(dia_cierre) != 0:
            fecha_hoy = date.today()
            dia_fin_mes = self.last_day_of_month(fecha_hoy).strftime('%d')
            if int(dia_cierre) > int(dia_fin_mes):
                dia_cierre = dia_fin_mes
            fecha_actual = datetime.now()
            anio_mes_actual = fecha_actual.strftime('%Y-%m')
            fecha_actual_corte = anio_mes_actual + '-' + str(dia_cierre)
            fecha_actual_corte_date = datetime.strptime(fecha_actual_corte, '%Y-%m-%d').date()
            if fecha_hoy >= fecha_actual_corte_date:
                mensaje_error = 'No es posible realizar esta operacion, se encuentra fuera de la fecha limite para ' \
                                'movimientos: ' + str(fecha_actual_corte_date)
                raise AccessError(
                    mensaje_error)
            #

        if self.state != 'val':
            self.state = 'val'
            json_seq_codes = {'112': 'saf-activos-fijos.transfer_cue',
                              '114': 'saf-activos-fijos.transfer_gye',
                              '113': 'saf-activos-fijos.transfer_uio',
                              '111': 'saf-activos-fijos.transfer_rec'}
            codigo_centro_costo = self.sede_origen.codigo
            num_seq = json_seq_codes[codigo_centro_costo]

            if self.name == 'Nuevo':
                self.name = self.env['ir.sequence'].next_by_code(num_seq) or 'Nuevo'

            for activo in self.tra_activos_fijos:
                # Actualizamos la informacion del activo
                activo.write({
                    'custodio': self.tra_custodio_destino.id
                })
        else:
            raise UserError(
                'La Transferencia ya se encuentra en estado "Validado", no es posible realizar esta operacion...')

    def last_day_of_month(self, date):
        if date.month == 12:
            return date.replace(day=31)
        return date.replace(month=date.month + 1, day=1) - timedelta(days=1)

    def unlink(self):
        for obj_trans in self:
            if obj_trans.state == 'val':
                raise UserError(
                    'No es posible eliminar la transferencia que ya ha sido Validada...')
            return models.Model.unlink(self)

    @api.onchange('tra_custodio_origen')
    def _onchange_acf_codigo(self):
        self.tra_custodio_destino = None
        self.tra_activos_fijos = None

    @api.onchange('sede_origen')
    def _onchange_sede_origen(self):
        self.tra_custodio_origen = None
        self.tra_activos_fijos = None

    @api.onchange('sede_destino')
    def _onchange_sede_destino(self):
        self.tra_custodio_destino = None
