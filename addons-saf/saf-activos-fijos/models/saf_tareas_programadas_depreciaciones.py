# -*- coding: utf-8 -*-
import base64
import json

from odoo import models, fields, api
from datetime import datetime
import requests
import traceback
import pytz
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class TareasProgramadasDepreciacionAF(models.Model):
    _name = 'saf.tareas_programadas_depreciacion_af'
    _description = 'Tareas Programadas Depreciacion AF'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(
        string='Descripción')
    move = fields.Many2one(
        "account.move",
        string="Asiento Contable",
        index=True
    )
    observaciones = fields.Text(
        string='Resultados')
    observaciones_manual = fields.Text(
        string='Observaciones')
    fecha_ejecucion_cron = fields.Date(
        string='Fecha ejecución',
        required=True)
    estado_ejecucion = fields.Selection(
        selection=[
            ('EJEC', "Ejecutado"),
            ('ERR', "Error")
        ],
        string='Estado',
        default='EJEC',
        help='Estado actual de la ejecución de la tarea programada')
    total_depreciaciones = fields.Integer(
        compute='_depreciaciones_total',
        string="Total Depreciaciones")
    total_bienes_control = fields.Integer(
        compute='_bienes_control_total',
        string="Total Bienes de Control")
    user_name = fields.Char(
        string='Nombre Usuario')
    tipo = fields.Selection(
        selection=[
            ('M', "Manual"),
            ('A', "Automática")
        ],
        string='Tipo Depreciación',
        default='M',
        help='Tipo de tarea Depreciación')
    centro_costo = fields.Many2one('account.analytic.plan',
                                   required=True,
                                   string='Centro de costo',
                                   help='Centro de costo de la depreciación'
                                   )
    generador_depreciacion = fields.Many2one(
        comodel_name='saf.generador.depreciaciones',
        string="Generador depreciaciones",
        ondelete='cascade')

    # def init(self):
    #     # Necesario para que tome el formato
    #     self.env.company.write({
    #         'paperformat_id': self.env.ref('saf-plantillas-reportes.format_a4_ups_header_customize').id,
    #     })
    #     sup = super(TareasProgramadasDepreciacionAF, self)
    #     if hasattr(sup, 'init'):
    #         sup.init()

    def btn_generar_depreciaciones(self):
        ref_datetime = self['fecha_ejecucion_cron']

        data_asiento = self.generar_depreciaciones(ref_datetime, self.centro_costo)
        val = {
            'observaciones': data_asiento['observaciones'],
            'estado_ejecucion': data_asiento['estado_tarea'],
            'move': data_asiento['move'].id if 'move' in data_asiento else None,
            'tipo': 'M',
            'name': self.centro_costo.name + " " + ref_datetime.strftime('%B') + ' del ' + ref_datetime.strftime('%Y'),
        }
        self.write(val)

        users_mail = self.centro_costo.users_depreciaciones
        self.send_mails_users(self, users_mail)

    def obtener_cuenta_tipo_activo(self, tipo_af, tipo_cuenta):
        cuenta = self.env['saf.tipo_activo'].search(
            [('taf_codigo', '=', tipo_af['id']), ('tac_tipo_cuenta', '=', tipo_cuenta)])
        return cuenta

    def send_mail_depreciaciones(self, data_cron, user, ids_adjuntos):
        email_values = {'email_cc': False, 'auto_delete': True, 'recipient_ids': [],
                        'partner_ids': [], 'scheduled_date': False,
                        'email_to': user.email}

        data_cron.write({'user_name': user.name})
        try:
            force_send = not (self.env.context.get('import_file', False))
            template = self.env.ref('saf-plantillas-email.saf_depreciacion_mensual_mail',
                                    raise_if_not_found=False)
            template.attachment_ids = [(6, 0, ids_adjuntos)]
            template.send_mail(data_cron.id,
                               force_send=force_send,
                               raise_exception=True, email_values=email_values)
        except ValueError:
            pass

    # Prepara el archivo que va adjuntado con el email
    def prepare_data_archivo_email(self, name, id_email, tareas_programada):
        # "Reporte Depreciaciones.pdf"
        # "saf-activos-fijos.action_report_tareas_programadas"
        report_template_id = self.env['ir.actions.report']._render_qweb_pdf(
            id_email, tareas_programada.id)

        data_record = base64.b64encode(report_template_id[0])
        ir_values = {
            'name': name,
            'type': 'binary',
            'datas': data_record,
            'store_fname': data_record,
            'mimetype': 'application/pdf',
        }
        data_id = self.env['ir.attachment'].create(ir_values)
        return data_id

    def send_mail_depreciaciones_button(self):
        users = self.centro_costo.users_depreciaciones
        name = "Reporte Depreciaciones.pdf"
        id_plantilla = "saf-activos-fijos.action_report_tareas_programadas"
        data_email = self.prepare_data_archivo_email(name, id_plantilla, self)

        name_pl2 = "Resumen contable por grupo.pdf"
        id_plantilla_2 = "saf-activos-fijos.action_report_tareas_programadas_group"
        data_email_2 = self.prepare_data_archivo_email(name_pl2, id_plantilla_2, self)

        for user in users:
            email_values = {'email_cc': False, 'auto_delete': True, 'recipient_ids': [],
                            'partner_ids': [], 'scheduled_date': False,
                            'email_to': user.email}

            self.write({'user_name': user.name})

            force_send = not (self.env.context.get('import_file', False))
            template = self.env.ref('saf-plantillas-email.saf_depreciacion_mensual_mail',
                                    raise_if_not_found=False)

            template.attachment_ids = [(6, 0, [data_email.id, data_email_2.id])]

            template.send_mail(self.id,
                               force_send=force_send,
                               raise_exception=True, email_values=email_values)

    def generar_depreciaciones(self, ref_datetime, centro_costo):
        data_asiento = {
            'estado_tarea': 'EJEC',
            'asiento_generado': False
        }
        detalles_asientos = []
        try:

            depreciaciones_hoy = self.env['saf.depeciacion_activo_fijo']. \
                search([('daf_periodo_depreciacion', '=', ref_datetime),
                        ('daf_status', '=', 'PEN'),
                        ('acf_codigo.state', '=', 'val'),
                        ('acf_codigo.centro_costo_uno', '=', centro_costo.id),
                        ('acf_codigo.acf_fecha_alta', '<=', ref_datetime)],
                       order="id desc")

            ir_default = self.env['ir.default'].sudo()
            diario_contable = ir_default.get('res.config.settings', 'diario_contable')

            if len(depreciaciones_hoy) > 0:
                move_vals = {
                    'journal_id': int(diario_contable),
                    'company_id': self.env.company.id,
                    'ref': 'DEPRECIACIÓN ' + centro_costo.name.upper(),
                    'date': ref_datetime,
                    'move_type': 'entry'
                }
                move = self.env['account.move'].create(move_vals)
                data_asiento['asiento_generado'] = True

            ban_depreciacion_existe = False

            for i, depreciacion in enumerate(depreciaciones_hoy):
                if depreciacion.acf_codigo.state != 'val':
                    continue

                # if depreciacion.acf_codigo.acf_fecha_alta != depreciacion.acf_codigo.acf_fecha_inicio_depreciacion:
                #     len_deps, detalles_generados = self.obtener_depreciaciones_anteriores_fecha_inicio(
                #         depreciacion.acf_codigo, move)
                #
                #     if len_deps != 0:
                #         detalles_asiento = detalles_generados

                if depreciacion.acf_codigo.acf_fecha_alta != depreciacion.acf_codigo.acf_fecha_inicio_depreciacion:
                    len_deps, detalles_generados = self.obtener_depreciaciones_anteriores_fecha_inicio(
                        depreciacion.acf_codigo, detalles_asientos, move)
                    if len_deps != 0:
                        detalles_asientos = detalles_generados

                dep_anterior_datetime = datetime(
                    year=depreciacion.daf_periodo_depreciacion.year,
                    month=depreciacion.daf_periodo_depreciacion.month,
                    day=depreciacion.daf_periodo_depreciacion.day,
                )

                # Verificar si se ha depreciado el mes anterior
                dep_anterior = self.obtener_depreciacion_mes_anterior(depreciacion.acf_codigo.id,
                                                                      dep_anterior_datetime)

                if dep_anterior.daf_status == 'PEN':
                    continue

                ban_depreciacion_existe = True

                val_depreciacion = {
                    'daf_status': 'DEP',
                    'move': move.id
                }
                depreciacion.write(val_depreciacion)
                activo = depreciacion.acf_codigo
                tipo_af = activo.taf_codigo

                # Arama un clave unica para cada centro de costo dos y grupo contable
                clave_ccdos_gc = activo.centro_costo_dos.code + activo.taf_codigo.taf_codigo_contable
                obj_ccdos_gc = {'codigo': clave_ccdos_gc,
                                'tipo_af': activo.taf_codigo.id,
                                'tipo_af_name': activo.taf_codigo.name,
                                'ccdos': activo.centro_costo_dos.id,
                                'ccdos_name': activo.centro_costo_dos.name,
                                'valor': round(depreciacion.daf_depreciacion_mes, 3)}

                # Si es la primera iteracion se agrega un objeto inicial
                if len(detalles_asientos) == 0:
                    detalles_asientos.append(obj_ccdos_gc)
                else:

                    # Recorre los detalles generados hasta el momento, y si se encuentra le suma el valor de
                    # la depreciacion
                    codigo_encontrado = False
                    for item in detalles_asientos:
                        if item["codigo"] == clave_ccdos_gc:
                            item['valor'] += round(depreciacion.daf_depreciacion_mes, 3)
                            codigo_encontrado = True
                            break

                    # En el caso de no encontrar agrega un nuevo elemento al detalle de los asientos
                    if codigo_encontrado is False:
                        detalles_asientos.append(obj_ccdos_gc)

                # cuenta_activo = self.obtener_cuenta_tipo_activo(tipo_af, 'ACTIVO')
                # cuenta_depreciacion = self.obtener_cuenta_tipo_activo(tipo_af, 'DEPRECIACION')
                #
                vida_util_res_af = activo.acf_vida_util_residual - 1

                val_af = {
                    'acf_vida_util_residual': vida_util_res_af
                }

                if depreciacion.daf_vida_util_residual == 0:
                    val_af['state'] = 'control'

                activo.write_depreciacion(val_af)

            detalles_to_create = []
            for detalle_asiento in detalles_asientos:

                cuenta_activo = self.obtener_cuenta_tipo_activo({'id': detalle_asiento['tipo_af']},
                                                                'ACTIVO')
                cuenta_depreciacion = self.obtener_cuenta_tipo_activo({'id': detalle_asiento['tipo_af']}
                                                                      , 'DEPRECIACION')

                etiqueta_detalle_asiento = detalle_asiento['ccdos_name'] + ' - ' \
                                           + detalle_asiento['tipo_af_name'] + '/' \
                                           + str(depreciacion.daf_periodo_depreciacion)

                debito = (0, 0, {
                    'name': etiqueta_detalle_asiento,
                    'account_id': cuenta_depreciacion[0].cuc_codigo.id,
                    'debit': round(detalle_asiento['valor'], 3),
                    'credit': 0,
                    'centro_costo': centro_costo.id,
                    'analytic_distribution': {
                        str(detalle_asiento['ccdos']): 100.0
                    }
                })

                credito = (0, 0, {
                    'name': etiqueta_detalle_asiento,
                    'account_id': cuenta_activo[0].cuc_codigo.id,
                    'debit': 0,
                    'credit': round(detalle_asiento['valor'], 3),
                    'centro_costo': centro_costo.id,
                })

                detalles_to_create.append(debito)
                detalles_to_create.append(credito)

            data_asiento['observaciones'] = 'Ejecutado exitosamente'
            if ban_depreciacion_existe is False:
                data_asiento['observaciones'] = 'Ejecutado exitosamente, sin embargo no ' \
                                                'existen activos que se deprecien en este periodo ' \
                                                'y no se generaron asientos contables'
            else:
                move.write({'line_ids': detalles_to_create})
                data_asiento['move'] = move

        except Exception:
            data_asiento['observaciones'] = 'Error ' + traceback.format_exc()
            data_asiento['estado_tarea'] = 'ERR'
            traceback.print_exc()
            pass

        return data_asiento

    # def obtener_depreciaciones_anteriores_fecha_inicio(self, activo, move):
    def obtener_depreciaciones_anteriores_fecha_inicio(self, activo, detalles_asientos, move):
        depreciaciones_anteriores = self.env['saf.depeciacion_activo_fijo']. \
            search([('daf_periodo_depreciacion', '<', activo.acf_fecha_alta),
                    ('daf_status', '=', 'PEN'),
                    ('acf_codigo.state', '=', 'val'),
                    ('acf_codigo', '=', activo.id)])

        if len(depreciaciones_anteriores) == 0:
            return len(depreciaciones_anteriores), []

        for i, depreciacion in enumerate(depreciaciones_anteriores):
            val_depreciacion = {
                'daf_status': 'DEP',
                'move': move.id
            }
            depreciacion.write(val_depreciacion)
            activo = depreciacion.acf_codigo

            # Arama un clave unica para cada centro de costo dos y grupo contable
            clave_ccdos_gc = activo.centro_costo_dos.code + activo.taf_codigo.taf_codigo_contable
            obj_ccdos_gc = {'codigo': clave_ccdos_gc,
                            'tipo_af': activo.taf_codigo.id,
                            'tipo_af_name': activo.taf_codigo.name,
                            'ccdos': activo.centro_costo_dos.id,
                            'ccdos_name': activo.centro_costo_dos.name,
                            'valor': round(depreciacion.daf_depreciacion_mes, 3)}
            if len(detalles_asientos) == 0:
                detalles_asientos.append(obj_ccdos_gc)
            else:

                # Recorre los detalles generados hasta el momento, y si se encuentra le suma el valor de
                # la depreciacion
                codigo_encontrado = False
                for item in detalles_asientos:
                    if item["codigo"] == clave_ccdos_gc:
                        item['valor'] += round(depreciacion.daf_depreciacion_mes, 3)
                        codigo_encontrado = True
                        break

                # En el caso de no encontrar agrega un nuevo elemento al detalle de los asientos
                if codigo_encontrado is False:
                    detalles_asientos.append(obj_ccdos_gc)

            # cuenta_activo = self.obtener_cuenta_tipo_activo(tipo_af, 'ACTIVO')
            # cuenta_depreciacion = self.obtener_cuenta_tipo_activo(tipo_af, 'DEPRECIACION')

            vida_util_res_af = activo.acf_vida_util_residual - 1

            val_af = {
                'acf_vida_util_residual': vida_util_res_af
            }
            if depreciacion.daf_vida_util_residual == 0:
                val_af['state'] = 'control'

            activo.write_depreciacion(val_af)

            # etiqueta_detalle_asiento = activo.searching + ' - ' + str(
            #     depreciacion.daf_periodo_depreciacion) + ' - ' + str(
            #     depreciacion.acf_codigo.acf_vida_util_restante - depreciacion.daf_vida_util_residual) + '/' + str(
            #     depreciacion.acf_codigo.acf_vida_util_restante)
            #
            # debito = (0, 0, {
            #     'name': etiqueta_detalle_asiento,
            #     'account_id': cuenta_activo[0].cuc_codigo.id,
            #     'debit': depreciacion.daf_depreciacion_mes,
            #     'credit': 0,
            #     'centro_costo': activo.centro_costo_uno.id,
            #     'analytic_distribution': {
            #         str(activo.centro_costo_dos.id): 100.0
            #     }
            # })
            #
            # credito = (0, 0, {
            #     'name': etiqueta_detalle_asiento,
            #     'account_id': cuenta_depreciacion[0].cuc_codigo.id,
            #     'debit': 0,
            #     'credit': depreciacion.daf_depreciacion_mes,
            #     'centro_costo': activo.centro_costo_uno.id,
            #     'analytic_distribution': {
            #         str(activo.centro_costo_dos.id): 100.0
            #     }
            # })
            #
            # detalles_generados.append(debito)
            # detalles_generados.append(credito)

        return len(depreciaciones_anteriores), detalles_asientos

    def cron_depreciacion(self):
        ref_datetime = datetime.now()

        all_plans = self.env['account.analytic.plan']. \
            search([('company_id', '=', self.env.company.id)])

        for plan in all_plans:
            data_asiento = self.generar_depreciaciones(ref_datetime, plan)
            data_cron = {
                'observaciones': data_asiento['observaciones'],
                'estado_ejecucion': data_asiento['estado_tarea'],
                'move': data_asiento['move'].id if 'move' in data_asiento else None,
                'fecha_ejecucion_cron': ref_datetime,
                'tipo': 'A',
                'name': plan.name + " " + ref_datetime.strftime('%B') + ' del ' + ref_datetime.strftime('%Y'),
                'centro_costo': plan.id
            }
            tarea_creada = self.create(data_cron)
            users_mail = plan.users_depreciaciones
            self.send_mails_users(tarea_creada, users_mail)

    def send_mails_users(self, data_cron, users_mail):

        name = "Reporte Depreciaciones.pdf"
        id_plantilla = "saf-activos-fijos.action_report_tareas_programadas"
        data_email = self.prepare_data_archivo_email(name, id_plantilla, data_cron)

        name_pl2 = "Resumen contable por grupo.pdf"
        id_plantilla_2 = "saf-activos-fijos.action_report_tareas_programadas_group"
        data_email_2 = self.prepare_data_archivo_email(name_pl2, id_plantilla_2, data_cron)

        ids_adjuntos = [data_email.id, data_email_2.id]

        for user in users_mail:
            self.send_mail_depreciaciones(data_cron, user, ids_adjuntos)

    def get_contacto_centro_costo(self):
        contacto_centro_costo = self.env.user.centro_costo_predeterminado
        return contacto_centro_costo

    def url_resultados(self):
        # Obtiene el id de la action y del menu para poder enviarlo en la URL
        action_tarea = self.env.ref('saf-activos-fijos.action_tareas_dep_af')
        menu_tarea = self.env.ref('saf-activos-fijos.menu_tareas_programadas')
        web_url_base = self.env['ir.config_parameter'].get_param('web.base.url')
        url_resultados = web_url_base + '/web#id=' + str(
            self.id) + '&cids=1&menu_id=' + str(menu_tarea.id) + '&action=' + str(
            action_tarea.id) + '&model=saf.tareas_programadas_depreciacion_af&view_type=form'
        return url_resultados

    # Convierte la fecha a la zona horaria del usuario
    def get_data_fecha(self):
        date_ejecucion = self.fecha_ejecucion_cron.strftime("%Y-%m-%d %H:%M:%S")
        user_tz = pytz.timezone(self.env.context.get('tz') or self.env.user.tz or 'UTC')
        date_tz = pytz.UTC.localize(datetime.strptime(date_ejecucion, DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(
            user_tz)
        date_format = date_tz.strftime("%Y-%m-%d %H:%M:%S")
        return date_format

    def _depreciaciones_total(self):
        for rec in self:
            depreciaciones_realizadas = self.env['saf.depeciacion_activo_fijo']. \
                search_count([('move', '=', self.move.id),
                              ('move', '!=', None),
                              ('daf_status', '=', 'DEP')])
            rec.total_depreciaciones = depreciaciones_realizadas

    def _bienes_control_total(self):
        for rec in self:
            depreciaciones_realizadas_bc = self.env['saf.depeciacion_activo_fijo']. \
                search_count([('move', '=', self.move.id),
                              ('move', '!=', None),
                              ('daf_status', '=', 'DEP'),
                              ('acf_codigo.state', '=', 'control')])
            rec.total_bienes_control = depreciaciones_realizadas_bc

    def action_depreciaciones_id(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("saf-activos-fijos.action_depreciaciones")
        depreciaciones_realizadas = self.env['saf.depeciacion_activo_fijo']. \
            search([('move', '=', self.move.id),
                    ('daf_status', '=', 'DEP')])

        ids_depreciaciones = [depreciacion.id for depreciacion in depreciaciones_realizadas]
        action['domain'] = [
            ('id', 'in', ids_depreciaciones)
        ]
        action['context'] = {'group_by': 'taf_codigo'}
        return action

    def action_bienes_control_id(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("saf-activos-fijos.action_activo_fijo")
        depreciaciones_realizadas = self.env['saf.depeciacion_activo_fijo']. \
            search([('move', '=', self.move.id),
                    ('daf_status', '=', 'DEP'),
                    ('acf_codigo.state', '=', 'control')])

        ids_activos = [depreciacion.acf_codigo.id for depreciacion in depreciaciones_realizadas]
        action['domain'] = [
            ('id', 'in', ids_activos),
            ('state', '=', 'control')
        ]
        return action

    def obtener_resumen_grupo_contable(self):
        tipos_af = self.env['saf.depeciacion_activo_fijo']. \
            search([('move', '=', self.move.id),
                    ('daf_status', '=', 'DEP')],
                   order="taf_codigo").mapped('taf_codigo')

        tipos_af_depreciaciones = []

        for tipo_af in tipos_af:
            cuenta_activo = self.obtener_cuenta_tipo_activo(tipo_af, 'ACTIVO')
            cuenta_depreciacion = self.obtener_cuenta_tipo_activo(tipo_af, 'DEPRECIACION')

            tipo_af_json = {
                'cuenta_activo': cuenta_activo.cuc_codigo.name,
                'cuenta_depreciacion': cuenta_depreciacion.cuc_codigo.name,
                'cuenta_activo_codigo': cuenta_activo.cuc_codigo.code,
                'cuenta_depreciacion_codigo': cuenta_depreciacion.cuc_codigo.code,
                'taf_codigo': tipo_af.taf_codigo_contable,
                'taf_name': tipo_af.name,
                'count_items': 0,
                'count_val_alta': 0,
                'count_dep_init': 0,
                'count_dep_mes': 0,
                'count_dep_acum': 0,
                'count_val_residual': 0,
                'centros_costo_2': []
            }
            groups = self.env['saf.depeciacion_activo_fijo']. \
                read_group([('move', '=', self.move.id),
                            ('daf_status', '=', 'DEP'),
                            ('taf_codigo', '=', tipo_af.id)],
                           ['daf_depreciacion_mes', 'centro_costo_dos', 'daf_valor_inicial_activo',
                            'daf_depreciacion_acum', 'daf_valor_residual'],
                           ['centro_costo_dos'])

            for group in groups:
                count_dep_inicial = group['daf_depreciacion_acum'] - group['daf_depreciacion_mes']
                group_json = {
                    'centro_costo_name': str(group['centro_costo_dos'][1]),
                    'count': group['centro_costo_dos_count'],
                    'count_val_alta': group['daf_valor_inicial_activo'],
                    'count_dep_mes': round(group['daf_depreciacion_mes'], 3),
                    'count_dep_acum': round(group['daf_depreciacion_acum'], 3),
                    'count_valor_residual': group['daf_valor_residual'],
                    'count_dep_inicial': count_dep_inicial
                }

                tipo_af_json['count_items'] += group['centro_costo_dos_count']
                tipo_af_json['count_val_alta'] += group['daf_valor_inicial_activo']
                tipo_af_json['count_dep_init'] += count_dep_inicial
                tipo_af_json['count_dep_mes'] += round(group['daf_depreciacion_mes'], 3)
                tipo_af_json['count_dep_acum'] += round(group['daf_depreciacion_acum'], 3)
                tipo_af_json['count_val_residual'] += group['daf_valor_residual']
                tipo_af_json['centros_costo_2'].append(group_json)

            tipos_af_depreciaciones.append(tipo_af_json)

        return tipos_af_depreciaciones

    def obtener_depreciacion_mes_anterior(self, activo_id, fecha_dep):

        # depreciaciones_hoy = self.env['saf.depeciacion_activo_fijo']. \
        #     search([('daf_periodo_depreciacion', '=', ref_datetime),
        #             ('daf_status', '=', 'PEN'),
        #             ('acf_codigo.state', '=', 'val'),
        #             ('acf_codigo.centro_costo_uno', '=', centro_costo.id)])

        depreciacion_anterior = self.env['saf.depeciacion_activo_fijo']. \
            search([('daf_periodo_depreciacion', '<', fecha_dep),
                    ('acf_codigo', '=', activo_id)],
                   order='id desc',
                   limit=1)
        return depreciacion_anterior

    def obtener_activos_por_asiento(self):

        tipos_af = self.env['saf.depeciacion_activo_fijo']. \
            search([('move', '=', self.move.id),
                    ('daf_status', '=', 'DEP')],
                   order="taf_codigo").mapped('taf_codigo')

        tipos_af_activos = []
        for tipo_af in tipos_af:
            cuenta_activo = self.obtener_cuenta_tipo_activo(tipo_af, 'ACTIVO')

            tipo_af_json = {
                'nombre_tipo_af': tipo_af.name,
                'cuenta_activo': str(cuenta_activo[0].cuc_codigo.code) + ' ' + cuenta_activo[0].cuc_codigo.name.upper()
            }

            activos = []
            depreciaciones = self.env['saf.depeciacion_activo_fijo']. \
                search([('move', '=', self.move.id),
                        ('daf_status', '=', 'DEP'),
                        ('taf_codigo', '=', tipo_af.id)])

            for depreciacion in depreciaciones:
                depreciacion_json = {
                    'nombre_af': depreciacion.acf_codigo.name,
                    'fecha_alta': depreciacion.acf_codigo.acf_fecha_alta,
                    'codigo_af': depreciacion.acf_codigo.acf_codigo_activo,
                    'periodo': depreciacion.daf_periodo_depreciacion,
                    'valor_inicial': depreciacion.daf_valor_inicial_activo,
                    'dep_mes': round(depreciacion.daf_depreciacion_mes, 3),
                    'dep_acum': depreciacion.daf_depreciacion_acum,
                    'valor_residual': depreciacion.daf_valor_residual,
                    'asiento': depreciacion.move.name,
                    'vida_util_restante': depreciacion.daf_vida_util_residual,
                    'vida_util_inicial': depreciacion.acf_codigo.acf_vida_util_restante,
                    'custodio_cedula': depreciacion.acf_codigo.custodio.identification_id
                }
                activos.append(depreciacion_json)

            tipo_af_json['activos'] = activos
            tipos_af_activos.append(tipo_af_json)

        return tipos_af_activos
