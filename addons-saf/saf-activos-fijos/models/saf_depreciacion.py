# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
import requests


class DepreciacionActivoFijo(models.Model):
    _name = 'saf.depeciacion_activo_fijo'
    _order = 'acf_codigo desc, daf_periodo_depreciacion asc'
    _description = 'Depreciacion Activo Fijo'
    acf_codigo = fields.Many2one(
        string='Activo Fijo',
        comodel_name='saf.activo_fijo',
        ondelete='cascade',
        index=True)
    asistente_id = fields.Many2one(
        string='Activo Fijo',
        comodel_name='saf.asistente_activo_fijo',
        ondelete='cascade',
        index=True
    )
    mon_codigo = fields.Many2one(
        string='Moneda',
        comodel_name='res.currency',
        required=True)
    daf_status = fields.Selection(
        selection=[
            ('DEP', "Depreciado"),
            ('PEN', "Pendiente"),
            ('REF', 'Referencia')
        ],
        string='Estado',
        default='PEN',
        help='Estado actual de la depreciación')
    daf_periodo_depreciacion = fields.Date(
        string='Periodo',
        required=True,
        index=True)
    daf_vida_util_residual = fields.Integer(
        string='Vida Util Res.',
        required=True)
    daf_valor_inicial_activo = fields.Float(
        string='Valor Inicial',
        digits=(16, 2),
        required=True)
    daf_depreciacion_mes = fields.Float(
        string='Dep. Mes',
        digits=(16, 3),
        required=True)
    daf_depreciacion_acum = fields.Float(
        string='Dep. Acum.',
        digits=(16, 3),
        required=True)
    daf_valor_residual = fields.Float(
        string='Valor Residual',
        digits=(16, 3),
        required=True)
    company_id = fields.Many2one(
        "res.company",
        string="Company",
        default=lambda self: self.env.company,
    )
    move = fields.Many2one(
        "account.move",
        string="Asiento Contable"
    )
    taf_codigo = fields.Many2one(
        string='Tipo Activo Fijo',
        compute='obtener_tipo_af',
        store=True,
        comodel_name='saf.tipo_activo_fijo')

    # Datos para calcular las depreciaciones faltantes
    daf_dep_final = fields.Float(
        string='Depreciacion Final',
        digits=(16, 3))
    daf_dep_inicial = fields.Float(
        string='Depreciacion Inicial',
        digits=(16, 3))
    daf_dep_acum_ini_anio = fields.Float(
        string='Depreciacion acumumlada inicio anio',
        digits=(16, 3))
    daf_vida_util_res_ini_anio = fields.Float(
        string='Depreciacion vida util residual inicio anio',
        digits=(16, 3))
    daf_mes_ini_anio = fields.Date(
        string='Depreciacion mes inicial anio')
    daf_vlr_corr_ini_anio = fields.Float(
        string='Vlr Corr Ini Anio',
        digits=(16, 3))
    ###

    centro_costo_uno = fields.Many2one(
        string='Centro costo 1',
        compute='obtener_centro_costo_uno',
        store=True,
        comodel_name='account.analytic.plan')
    centro_costo_dos = fields.Many2one(
        string='Centro costo 2',
        compute='obtener_centro_costo_dos',
        store=True,
        comodel_name='account.analytic.account')

    @api.depends('acf_codigo')
    def obtener_tipo_af(self):
        for rec in self:
            if len(rec.asistente_id) == 0:
                rec.taf_codigo = rec.acf_codigo.taf_codigo.id

            else:
                rec.taf_codigo = None

    @api.depends('acf_codigo')
    def obtener_centro_costo_uno(self):
        for rec in self:
            if len(rec.asistente_id) == 0:
                rec.centro_costo_uno = rec.acf_codigo.centro_costo_uno.id

            else:
                rec.centro_costo_uno = None

    @api.depends('acf_codigo')
    def obtener_centro_costo_dos(self):
        for rec in self:
            if len(rec.asistente_id) == 0:
                rec.centro_costo_dos = rec.acf_codigo.centro_costo_dos.id

            else:
                rec.centro_costo_dos = None
