# -*- coding: utf-8 -*-

from . import saf_tareas_programadas_depreciaciones
from . import saf_tipo_activo_af
from . import saf_tipo_activo_cuenta
from . import saf_caracteristica
from . import saf_caracateristica_tipo_af
from . import saf_lista_caracteristica
from . import saf_asistente_af
from . import saf_activo_fijo
from . import saf_caracteristica_af
from . import saf_tipo_poliza
from . import saf_poliza_af
from . import saf_motivo_baja
from . import saf_depreciacion
from . import saf_baja
from . import saf_transferencia
from . import saf_ubicaciones_organicas
from . import settings
from . import saf_generador_depreciaciones
