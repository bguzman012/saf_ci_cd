# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError


class BajaActivoFijo(models.Model):
    _name = 'saf.baja_activo_fijo'
    _description = 'Baja Activo Fijo'
    _rec_name = 'name'
    _order = 'state asc, baf_fecha desc'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(
        copy=False,
        default="Nuevo",
        string="Código",
        readonly=True)
    acf_lista_activos = fields.Many2many(
        comodel_name='saf.activo_fijo',
        string='Activos Fijos')
    baf_fecha = fields.Date(
        string='Fecha de baja',
        required=True,
        default=fields.Date.today())
    baf_observaciones = fields.Text(
        string='Observaciones')
    done = fields.Boolean(
        string='Realizada',
        readonly=True)
    state = fields.Selection(
        selection=[
            ('borr', "Borrador"),
            ('val', "Validado")
        ],
        string='Estado',
        default='borr',
        tracking=True,
        help='Estado actual del activo')
    baf_custodio = fields.Many2one(
        comodel_name='hr.employee',
        string="Custodio",
        required=True)
    company_id = fields.Many2one(
        "res.company",
        string="Company",
        default=lambda self: self.env.company)
    sede = fields.Many2one(
        "account.analytic.plan",
        string="Sede",
        domain=lambda self: self._get_allowed_centros_costo(),
        default=lambda self: self.env.user.centro_costo_predeterminado,
        required=True
    )
    move = fields.Many2one(
        "account.move",
        string="Asiento Contable"
    )
    motivo = fields.Many2one(
        comodel_name='saf.motivos_baja',
        string="Motivo",
        required=True)

    @api.onchange('sede')
    def _onchange_sede_origen(self):
        self.baf_custodio = None
        self.acf_lista_activos = None

    def _get_allowed_centros_costo(self):
        allowed_centros_costo = self.env.user.centros_costo.ids
        return [('id', 'in', allowed_centros_costo)]

    def init(self):

        # Necesario para que tome el formato
        self.env.company.write({
            'paperformat_id': self.env.ref('saf-plantillas-reportes.format_a4_ups').id,
        })
        sup = super(BajaActivoFijo, self)
        if hasattr(sup, 'init'):
            sup.init()

    def validar(self):
        baja_activo_fijo = self.baf_fecha
        detalles_asiento = []
        ir_default = self.env['ir.default'].sudo()
        diario_contable = ir_default.get('res.config.settings', 'diario_contable_bajas')

        if len(self.acf_lista_activos) > 0:
            move_vals = {
                'journal_id': int(diario_contable),
                'company_id': self.env.company.id,
                'ref': 'BAJA',
                'date': self.baf_fecha,
                'move_type': 'entry'
            }
            detalles_asiento = []
            move = self.env['account.move'].create(move_vals)

        for activo in self.acf_lista_activos:
            activo_fijo_valor_alta = activo.acf_valor_alta
            lista_depreciaciones = self.env['saf.depeciacion_activo_fijo'].search(
                [('acf_codigo', '=', activo.id),
                 ('daf_periodo_depreciacion', '<=', baja_activo_fijo)],
                order='id desc',
                limit=1)

            if len(lista_depreciaciones) > 0:
                depreciacion_cercana = lista_depreciaciones[0]

                lista_depreciaciones_prox = self.env['saf.depeciacion_activo_fijo'].search(
                    [('acf_codigo', '=', activo.id),
                     ('daf_periodo_depreciacion', '>', depreciacion_cercana['daf_periodo_depreciacion'])],
                    order='id asc',
                    limit=1)

                if len(lista_depreciaciones_prox) > 0:
                    depreciacion_proxima = lista_depreciaciones_prox[0]
                    num_dias = abs(depreciacion_proxima['daf_periodo_depreciacion'] -
                                   depreciacion_cercana['daf_periodo_depreciacion']).days

                    num_dias_transcurridos = abs(baja_activo_fijo -
                                                 depreciacion_cercana['daf_periodo_depreciacion']).days

                    valor_baja_porcentaje = (num_dias_transcurridos *
                                             depreciacion_proxima['daf_depreciacion_mes']) / num_dias

                    baja_final = depreciacion_cercana['daf_depreciacion_acum'] + valor_baja_porcentaje

                    valor_baja_final = activo_fijo_valor_alta - baja_final

                    activo.write({
                        'acf_valor_baja': valor_baja_final,
                        'acf_fecha_baja': baja_activo_fijo,
                        'acf_vida_util_residual': 0,
                        'state': 'baj'
                    })
            else:
                lista_depreciaciones_prox = self.env['saf.depeciacion_activo_fijo'].search(
                    [('acf_codigo', '=', activo.id),
                     ('daf_periodo_depreciacion', '>', baja_activo_fijo)],
                    order='id asc',
                    limit=1)

                if len(lista_depreciaciones_prox) > 0:
                    depreciacion_proxima = lista_depreciaciones_prox[0]
                    num_dias_transcurridos = abs(depreciacion_proxima['daf_periodo_depreciacion'] -
                                                 activo.acf_fecha_alta).days

                    num_actual_baja_transcurrido = abs(baja_activo_fijo -
                                                       activo.acf_fecha_alta).days

                    valor_baja_porcentaje_transcurrido = (num_actual_baja_transcurrido *
                                                          depreciacion_proxima['daf_depreciacion_mes']) / \
                                                         num_dias_transcurridos

                    valor_baja_final = activo_fijo_valor_alta - valor_baja_porcentaje_transcurrido

                    activo.write({
                        'acf_valor_baja': valor_baja_final,
                        'acf_fecha_baja': baja_activo_fijo,
                        'acf_vida_util_residual': 0,
                        'state': 'baj'
                    })

            tipo_af = activo.taf_codigo
            cuenta_baja = self.obtener_cuenta_tipo_activo(tipo_af, 'BAJA')
            cuenta_depreciacion_acum = self.obtener_cuenta_tipo_activo(tipo_af, 'DEPRECIACION ACUMULADA')

            debito = (0, 0, {
                'name': activo.searching,
                'account_id': cuenta_baja[0].cuc_codigo.id,
                'debit': abs(activo.acf_valor_alta),
                'credit': 0,
                'centro_costo': activo.centro_costo_uno.id,
                'analytic_distribution': {
                    str(activo.centro_costo_dos.id): 100.0
                }
            })

            credito = (0, 0, {
                'name': activo.searching,
                'account_id': cuenta_depreciacion_acum[0].cuc_codigo.id,
                'debit': 0,
                'credit': abs(activo.acf_valor_alta),
                'centro_costo': activo.centro_costo_uno.id,
                'analytic_distribution': {
                    str(activo.centro_costo_dos.id): 100.0
                }
            })

            detalles_asiento.append(debito)
            detalles_asiento.append(credito)

        if len(self.acf_lista_activos) > 0:
            move.write({'line_ids': detalles_asiento})
            self.move = move

        if self.state != 'val':
            json_seq_codes = {'112': 'saf-activos-fijos.baja_cue',
                              '114': 'saf-activos-fijos.baja_gye',
                              '113': 'saf-activos-fijos.baja_uio',
                              '111': 'saf-activos-fijos.baja_rec'}
            codigo_centro_costo = self.sede.codigo
            num_seq = json_seq_codes[codigo_centro_costo]
            if self['name'] == 'Nuevo':
                self['name'] = self.env['ir.sequence'].next_by_code(num_seq) or 'Nuevo'

            self.state = 'val'

    def obtener_cuenta_tipo_activo(self, tipo_af, tipo_cuenta):
        cuenta = self.env['saf.tipo_activo'].search(
            [('taf_codigo', '=', tipo_af.id), ('tac_tipo_cuenta', '=', tipo_cuenta)])
        return cuenta

    @api.model
    def create(self, val):
        if len(val['acf_lista_activos'][0][2]) == 0:
            raise ValidationError('Por favor seleccione al menos un activo fijo...')

        bajas = super(BajaActivoFijo, self).create(val)

        return bajas

    def create_migracion(self, val):
        bajas = super(BajaActivoFijo, self).create(val)
        return bajas

    def unlink(self):
        for obj_baja in self:
            if obj_baja.state == 'val':
                raise UserError(
                    'No es posible eliminar la baja que ya ha sido Validada...')
            return models.Model.unlink(self)
