# -*- coding: utf-8 -*-

from odoo import models, fields


class TipoPoliza(models.Model):
    _name = 'saf.tipo_poliza'
    _description = 'Tipo Poliza'
    name = fields.Char(
        string='Tipo Descripcion',
        required=True)
