# -*- coding: utf-8 -*-
import base64
import json

from odoo import models, fields, api
from datetime import datetime
import requests
import traceback
import pytz
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class GeneradorDepreciaciones(models.Model):
    _name = 'saf.generador.depreciaciones'
    _description = 'Generador de transferencias'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(
        string='Descripción')
    observaciones = fields.Text(
        string='Resultados')
    fecha_ejecucion = fields.Date(
        string='Fecha ejecución',
        required=True)
    estado_ejecucion = fields.Selection(
        selection=[
            ('PEN', "Pendiente"),
            ('EJEC', "Ejecutado"),
            ('ERR', "Error")
        ],
        string='Estado',
        default='PEN',
        help='Estado actual de la ejecución de la depreciación')
    tareas_depreciaciones = fields.One2many(
        'saf.tareas_programadas_depreciacion_af',
        'generador_depreciacion',
        'Depreciaciones Generadas por Centro de Costo')

    def btn_generar_depreciaciones(self):
        try:
            centros_costo = self.env['account.analytic.plan']. \
                search([('company_id', '=', self.env.company.id)])

            for centro_costo in centros_costo:
                data_asiento = self.env['saf.tareas_programadas_depreciacion_af']. \
                    generar_depreciaciones(self.fecha_ejecucion, centro_costo)
                data_cron = {
                    'observaciones': data_asiento['observaciones'],
                    'estado_ejecucion': data_asiento['estado_tarea'],
                    'move': data_asiento['move'].id if 'move' in data_asiento else None,
                    'fecha_ejecucion_cron': self.fecha_ejecucion,
                    'tipo': 'M',
                    'name': centro_costo.name + " " + self.fecha_ejecucion.strftime(
                        '%B') + ' del ' + self.fecha_ejecucion.strftime('%Y'),
                    'centro_costo': centro_costo.id,
                    'generador_depreciacion': self.id
                }
                tarea_creada = self.env['saf.tareas_programadas_depreciacion_af'].create(data_cron)
                if 'move' in data_asiento:
                    users_mail = centro_costo.users_depreciaciones
                    self.env['saf.tareas_programadas_depreciacion_af']. \
                        send_mails_users(tarea_creada, users_mail)

            self.observaciones = 'Depreciaciones ejecutadas correctamente'
            self.estado_ejecucion = 'EJEC'

        except Exception:
            self.observaciones = 'Error ' + traceback.format_exc()
            self.estado_ejecucion = 'ERR'
            traceback.print_exc()
