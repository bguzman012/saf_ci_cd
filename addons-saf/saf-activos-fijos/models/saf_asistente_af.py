# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime, timedelta, date
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.fields import float_round


class AsistenteActivoFijo(models.Model):
    _name = 'saf.asistente_activo_fijo'
    _description = 'Asistente para la creacion masiva de activos fijos'
    _order = 'write_date desc'

    name = fields.Text(
        string='Descripcion',
        required=True)
    active = fields.Boolean(
        'Active',
        default=True)
    state = fields.Selection(
        selection=[
            ('borr', "Borrador"),
            ('val', "Validado")
        ],
        string='Estado',
        default='borr',
        tracking=True,
        help='Estado actual del activo')
    taf_codigo = fields.Many2one(
        string='Tipo Activo Fijo',
        comodel_name='saf.tipo_activo_fijo',
        required=True)
    centro_costo_uno = fields.Many2one(
        "account.analytic.plan",
        string="Centro de Costo 1",
        domain=lambda self: self._get_allowed_centros_costo(),
        default=lambda self: self.env.user.centro_costo_predeterminado,
        required=True
    )
    centro_costo_dos = fields.Many2one(
        "account.analytic.account",
        string="Centro de Costo 2",
        required=True
    )
    acf_fecha_alta = fields.Date(
        string='Fecha Alta',
        required=True,
        default=fields.Date.today())
    acf_fecha_inicio_depreciacion = fields.Date(
        string='Fecha Inicio Depreciación',
        required=True,
        help='Es necesario tener permisos necesarios (habilitado_cambio_inicio_depreciacion)'
             ' para modificar el inicio de depreciación',
        default=fields.Date.today())
    acf_valor_alta = fields.Monetary(
        string='Valor Alta',
        required=True)
    acf_valor_reposicion = fields.Monetary(
        string='Valor Reposición',
        required=True)
    acf_valor_revalorizacion = fields.Float(
        string='Valor Revalorización',
        digits=(16, 6))
    acf_fecha_baja = fields.Datetime(
        string='Fecha Baja')
    acf_unidad_ing = fields.Integer(
        string='Unidad Ing.',
        required=True)
    acf_codigo_barras = fields.Char(
        string='Codigo Barras',
        required=False)
    acf_vida_util_restante = fields.Integer(
        string='Vida Útil',
        required=True,
        help="La vida útil restante debe ser ingresada en meses")
    acf_factura_compra = fields.Char(
        string='Num. Factura')
    acf_fecha_compra = fields.Date(
        string='Fecha',
        default=fields.Date.today())
    emp_codigo = fields.Integer(
        string='Empleado')
    acf_custodio = fields.Integer(
        string='Custodio',
        required=False)
    activos_fijos = fields.One2many(
        'saf.activo_fijo',
        'afid',
        'Activos Creados')
    company_id = fields.Many2one(
        "res.company",
        string="Company",
        readonly=True,
        default=lambda self: self.env.company)
    currency_id = fields.Many2one(
        string='Moneda',
        comodel_name='res.currency',
        default=2)
    custodio = fields.Many2one(
        comodel_name='hr.employee',
        string="Custodio",
        ondelete='cascade',
        required=True)
    proveedor = fields.Many2one(
        comodel_name='res.partner',
        string="Proveedor",
        required=True)
    caracteristicas = fields.One2many(
        comodel_name='saf.caracteristica_activo_fijo',
        inverse_name='asistente_af_codigo',
        string='Caracteristicas de Activo Fijo')
    tiene_permisos_cambiar_vu = fields.Boolean(
        compute='_permisos_vida_util',
        string='Tiene permisos para el cambio de vida util?.'
    )
    tiene_permisos_cambiar_init_dep = fields.Boolean(
        compute='_permisos_init_dep',
        string='Tiene permisos para el cambio del inicio de depreciación?.'
    )
    depreciacion = fields.One2many(
        comodel_name='saf.depeciacion_activo_fijo',
        inverse_name='asistente_id',
        string='Depreciación de activo')

    def _permisos_init_dep(self):
        for rec in self:
            if self.env.user.has_group('saf-activos-fijos.habilitado_cambio_inicio_depreciacion'):
                rec.tiene_permisos_cambiar_init_dep = True
            else:
                rec.tiene_permisos_cambiar_init_dep = False

    @api.onchange('acf_fecha_alta')
    def _onchange_fecha_alta(self):
        for record in self:
            record.acf_fecha_inicio_depreciacion = record.acf_fecha_alta
            if self.env.user.has_group('saf-activos-fijos.habilitado_cambio_inicio_depreciacion'):
                record.tiene_permisos_cambiar_init_dep = True
            else:
                record.tiene_permisos_cambiar_init_dep = False

    def _permisos_vida_util(self):
        for rec in self:
            if self.env.user.has_group('saf-activos-fijos.habilitado_vida_util_cambio'):
                rec.tiene_permisos_cambiar_vu = True
            else:
                rec.tiene_permisos_cambiar_vu = False

    @api.constrains('acf_unidad_ing', 'acf_valor_alta')
    def _check_unidad_ing(self):
        for record in self:
            if record.acf_unidad_ing <= 0:
                raise ValidationError("El numero de unidades no puede ser menor o igual a 0")
            if record.acf_valor_alta <= 0:
                raise ValidationError("El valor de alta no puede ser menor o igual a 0")

    @api.onchange('taf_codigo')
    def _onchange_taf(self):
        for record in self:
            record.acf_vida_util_restante = record.taf_codigo.taf_vida_util
            if self.env.user.has_group('saf-activos-fijos.habilitado_vida_util_cambio'):
                record.tiene_permisos_cambiar_vu = True
            else:
                record.tiene_permisos_cambiar_vu = False

    @api.model
    def create(self, val):
        if not self.env.user.has_group('saf-activos-fijos.habilitado_vida_util_cambio'):
            vida_util_taf = self.env['saf.tipo_activo_fijo'].search(
                [('id', '=', val['taf_codigo'])])[0]['taf_vida_util']
            val['acf_vida_util_restante'] = vida_util_taf

        asistente = super(AsistenteActivoFijo, self).create(val)
        self.state = 'borr'

        lista_depreciaciones = self.calcular_tabla_depreciacion(asistente)

        for activo in range(val['acf_unidad_ing']):
            codigo_activo = "Borrador"
            cadena_searching = val['name'] + ' [' + str(codigo_activo) + ']'
            vals = {
                'afid': asistente.id,
                'centro_costo_uno': val['centro_costo_uno'],
                'centro_costo_dos': val['centro_costo_dos'],
                'name': val['name'],
                'searching': cadena_searching,
                'acf_codigo_activo': codigo_activo,
                'taf_codigo': val['taf_codigo'],
                'acf_fecha_alta': val['acf_fecha_alta'],
                'acf_fecha_inicio_depreciacion': val['acf_fecha_inicio_depreciacion'],
                'acf_valor_alta': val['acf_valor_alta'],
                'acf_vida_util_restante': val['acf_vida_util_restante'],
                'acf_vida_util_residual': val['acf_vida_util_restante'],
                'acf_valor_reposicion': val['acf_valor_reposicion'],
                'proveedor': val['proveedor'],
                'acf_valor_revalorizacion': val['acf_valor_revalorizacion'] if "acf_valor_revalorizacion" in dict.keys(
                    val) else None,
                'acf_factura_compra': val['acf_factura_compra'] if "acf_factura_compra" in dict.keys(
                    val) else None,
                'acf_fecha_compra': val['acf_fecha_compra'] if "acf_fecha_compra" in dict.keys(
                    val) else None,
                'custodio': val['custodio'],
                'caracteristicas': val['caracteristicas']
            }

            activo = self.env['saf.activo_fijo'].create(vals)

            for depreciacion in lista_depreciaciones:
                depreciacion['asistente_id'] = None
                depreciacion['acf_codigo'] = activo.id
                depreciacion['daf_status'] = 'PEN'
                self.env['saf.depeciacion_activo_fijo'].create(depreciacion)

        return asistente

    def calcular_tabla_depreciacion(self, activo_creado):

        moneda = self.env.company.currency_id.id
        moneda_precision_decimal = self.env.company.currency_id.decimal_places

        nperiodos = activo_creado.acf_vida_util_restante
        v_inicial = activo_creado.acf_valor_alta
        v_dep_final = 0

        v_util_residual = nperiodos
        v_util_ini_anio = nperiodos
        v_dep_acumulada = 0
        v_dep_acumulada_final = 0

        v_dep_mes = 0

        lista_depreciaciones = []

        if activo_creado.acf_fecha_inicio_depreciacion.day > 14:
            # Si el dia es mayor a 14, se crea la primera depreciacion el siguiente mes
            fecha_inicio_deps = activo_creado.acf_fecha_inicio_depreciacion
            if fecha_inicio_deps.day > 28:
                fecha_inicio_deps = fecha_inicio_deps.replace(day=28)
            carry, new_month = divmod(fecha_inicio_deps.month - 1 + 1, 12)
            new_month += 1
            fecha_inicio_dep = fecha_inicio_deps.replace(year=fecha_inicio_deps.year + carry, month=new_month)
            finmes = self.last_day_of_month(fecha_inicio_dep)
        else:
            finmes = self.last_day_of_month(activo_creado.acf_fecha_inicio_depreciacion)

        for periodo in range(nperiodos):
            if finmes.month == 1:
                v_dep_acumulada = 0
                v_dep_final = v_dep_acumulada_final
                v_util_ini_anio = v_util_residual
            else:
                v_dep_acumulada += v_dep_mes

            v_util_residual -= 1
            v_meses = v_util_ini_anio - v_util_residual
            dif_val_init_dep_fin = v_inicial - v_dep_final
            div_val_init_dep_fin_res_ini = dif_val_init_dep_fin / v_util_ini_anio
            mul_val_init_dep_fin_res_ini_meses = div_val_init_dep_fin_res_ini * v_meses
            v_dep_mes = mul_val_init_dep_fin_res_ini_meses - v_dep_acumulada
            v_dep_acumulada_final += v_dep_mes
            vals = {
                'asistente_id': activo_creado.id,
                'mon_codigo': moneda,
                'daf_periodo_depreciacion': finmes,
                'daf_vida_util_residual': v_util_residual,
                'daf_valor_inicial_activo': v_inicial,
                'daf_depreciacion_mes': float_round(v_dep_mes, precision_digits=moneda_precision_decimal),
                'daf_depreciacion_acum': float_round(v_dep_acumulada_final, precision_digits=moneda_precision_decimal),
                'daf_valor_residual': float_round(v_inicial - v_dep_acumulada_final,
                                                  precision_digits=moneda_precision_decimal),
                'daf_status': 'REF'
            }

            self.env['saf.depeciacion_activo_fijo'].create(vals)
            finmes = self.last_day_of_month(finmes + timedelta(days=1))
            lista_depreciaciones.append(vals)
        return lista_depreciaciones

        # v_inicial = activo_creado.acf_valor_alta
        # v_residual = activo_creado.acf_valor_reposicion
        # v_depreciar = v_inicial - v_residual
        # nperiodos = activo_creado.acf_vida_util_restante
        # moneda = 2
        #
        # depremes = round(v_depreciar / nperiodos, 2)
        #
        # finmes = self.last_day_of_month(activo_creado.acf_fecha_alta)
        # depreacu = depremes
        # vrestante = nperiodos
        # ajuste = 0
        # lista_depreciaciones = []
        # for periodo in range(nperiodos):
        #     if periodo == nperiodos - 1:
        #         ajuste = v_depreciar - depreacu
        #
        #     val_residual = v_depreciar - depreacu + ajuste
        #     vals = {
        #         'asistente_id': activo_creado.id,
        #         'mon_codigo': moneda,
        #         'daf_periodo_depreciacion': finmes,
        #         'daf_vida_util_residual': vrestante,
        #         'daf_valor_inicial_activo': v_depreciar,
        #         'daf_depreciacion_mes': depremes + ajuste,
        #         'daf_depreciacion_acum': depreacu + ajuste,
        #         'daf_valor_residual': val_residual,
        #         'daf_status': 'REF'
        #     }
        #     finmes = self.last_day_of_month(finmes + timedelta(days=1))
        #     vrestante = vrestante - 1
        #     depreacu = depreacu + depremes + ajuste
        #     self.env['saf.depeciacion_activo_fijo'].create(vals)
        #     lista_depreciaciones.append(vals)
        # return lista_depreciaciones

    def crear_numero_unidades_menor_existencias(self, len_activos_sv, unidades_ing):

        lista_activos = self.env['saf.activo_fijo'].search(
            [('afid', '=', self.id)], order='id desc')

        lista_activos_val_deprec = self.env['saf.activo_fijo'].search(
            [('afid', '=', self.id),
             ('state', '=', 'val'),
             ('num_depreciaciones_realizadas', '>', 0)], order='id desc')

        lista_size_total = len_activos_sv - len(lista_activos_val_deprec)

        while lista_size_total > unidades_ing:
            lista_size_total -= 1
            # if lista_activos[len_activos_sv - 1]['state'] == 'val' or \
            #         lista_activos[len_activos_sv - 1]['num_depreciaciones_realizadas'] > 0:
            #     continue

            lista_activos[lista_size_total - 1].unlink()

    def _get_allowed_centros_costo(self):
        allowed_centros_costo = self.env.user.centros_costo.ids
        return [('id', 'in', allowed_centros_costo)]

    def crear_numero_unidades_mayor_existencias(self, len_activos_sv, unidades_ing):
        range_af = range(len_activos_sv, unidades_ing)
        for index in range_af:
            codigo_activo = "Borrador"
            cadena_searching = self['name'] + ' [' + str(codigo_activo) + ']'
            val_activo = {
                'afid': self['id'],
                'centro_costo_uno': self['centro_costo_uno']['id'],
                'centro_costo_dos': self['centro_costo_dos']['id'],
                'name': self['name'],
                'searching': cadena_searching,
                'acf_codigo_activo': codigo_activo,
                'taf_codigo': self['taf_codigo']['id'],
                'acf_fecha_alta': self['acf_fecha_alta'],
                'acf_fecha_inicio_depreciacion': self['acf_fecha_inicio_depreciacion'],
                'acf_valor_alta': self['acf_valor_alta'],
                'acf_vida_util_restante': self['acf_vida_util_restante'],
                'acf_valor_reposicion': self['acf_valor_reposicion'],
                'acf_valor_revalorizacion': self['acf_valor_revalorizacion'] if self[
                    'acf_valor_revalorizacion'] else None,
                'acf_factura_compra': self['acf_factura_compra'] if self["acf_factura_compra"] else None,
                'acf_fecha_compra': self['acf_fecha_compra'] if self["acf_fecha_compra"] else None,
                'custodio': self['custodio']['id'],
                'proveedor': self['proveedor']['id']
            }
            self.env['saf.activo_fijo'].create(val_activo)

    def write(self, vals):
        if 'taf_codigo' not in vals:
            vals['taf_codigo'] = self.taf_codigo.id
        if 'acf_vida_util_restante' not in vals:
            vals['acf_vida_util_restante'] = self['acf_vida_util_restante']

        if self['tiene_permisos_cambiar_vu'] is False:
            vida_util_taf = self.env['saf.tipo_activo_fijo'].search(
                [('id', '=', vals['taf_codigo'])])[0]['taf_vida_util']
            vida_util_restante = vida_util_taf
        else:
            vida_util_restante = vals['acf_vida_util_restante']

        if self['acf_vida_util_restante'] == vida_util_restante:
            vals['acf_vida_util_restante'] = self['acf_vida_util_restante']
        else:
            vals['acf_vida_util_restante'] = vida_util_restante

        asistente = super(AsistenteActivoFijo, self).write(vals)

        depre_del = self.env['saf.depeciacion_activo_fijo'].search(
            [('asistente_id', '=', self.id)])
        depre_del.unlink()
        lista_depreciaciones = self.calcular_tabla_depreciacion(self)

        # Crea o eliminar registros dependiendo del cambio del numero de unidades ingresada
        if 'acf_unidad_ing' in vals:
            len_activos_sv = len(self.activos_fijos)
            unidades_ing = vals['acf_unidad_ing']
            if unidades_ing > len_activos_sv:
                self.crear_numero_unidades_mayor_existencias(len_activos_sv, unidades_ing)

            len_activos_sv = len(self.activos_fijos)
            unidades_ing = vals['acf_unidad_ing']
            if unidades_ing < len_activos_sv:
                self.crear_numero_unidades_menor_existencias(len_activos_sv, unidades_ing)

        for activo in self.activos_fijos:
            if activo['state'] == 'val':
                continue

            if activo['num_depreciaciones_realizadas'] > 0:
                # Actualizar la informacion del activo
                val_activo = {
                    'centro_costo_uno': self['centro_costo_uno']['id'],
                    'centro_costo_dos': self['centro_costo_dos']['id'],
                    'name': self['name'],
                    'acf_factura_compra': self['acf_factura_compra'] if self["acf_factura_compra"] else None,
                    'acf_fecha_compra': self['acf_fecha_compra'] if self["acf_fecha_compra"] else None,
                    # 'custodio': self['custodio']['id'],
                    'proveedor': self['proveedor']['id']
                }
                activo.write(val_activo)
                continue

            codigo_activo = "Borrador"
            cadena_searching = self['name'] + ' [' + str(codigo_activo) + ']'

            # Elimina todas las caracteristicas que han sido creadas de forma masiva
            # para evitar perder informacion de numeros de serie, por ejemplo
            lista_caracteristicas = self.env['saf.caracteristica_activo_fijo'].search(
                [('acf_codigo', '=', activo.id),
                 ('caf_creada_masivamente', '=', True)])
            lista_caracteristicas.unlink()

            # Crea nuevamente las caracteristicas con la informacion nueva asignada de forma masiva
            for caracteristica in self.caracteristicas:
                val_car = {
                    'caf_creada_masivamente': True,
                    'acf_codigo': activo.id,
                    'cta_codigo': caracteristica['cta_codigo']['id'],
                    'lic_codigo': caracteristica['lic_codigo']['id'],
                    'caf_descripcion_texto': caracteristica['caf_descripcion_texto'],
                    'caf_descripcion_numero': caracteristica['caf_descripcion_numero'],
                    'caf_descripcion_fecha': caracteristica['caf_descripcion_fecha'],
                    'caf_tipo_cta_codigo': caracteristica['caf_tipo_cta_codigo'],
                    'caf_valor_cadena_texto': caracteristica['caf_valor_cadena_texto']
                }
                self.env['saf.caracteristica_activo_fijo'].create(val_car)

            # Elimina la lista de depreciaciones actual
            depre_del = self.env['saf.depeciacion_activo_fijo'].search(
                [('acf_codigo', '=', activo.id)])
            depre_del.unlink()

            # Genera con los datos ingresados la lista de depreciaciones
            for depreciacion in lista_depreciaciones:
                depreciacion['asistente_id'] = None
                depreciacion['acf_codigo'] = activo.id
                depreciacion['daf_status'] = 'PEN'
                self.env['saf.depeciacion_activo_fijo'].create(depreciacion)

            # Actualizar la informacion del activo
            val_activo = {
                'centro_costo_uno': self['centro_costo_uno']['id'],
                'centro_costo_dos': self['centro_costo_dos']['id'],
                'name': self['name'],
                'searching': cadena_searching,
                'acf_codigo_activo': codigo_activo,
                'taf_codigo': self['taf_codigo']['id'],
                'acf_fecha_alta': self['acf_fecha_alta'],
                'acf_fecha_inicio_depreciacion': self['acf_fecha_inicio_depreciacion'],
                'acf_valor_alta': self['acf_valor_alta'],
                'acf_vida_util_restante': self['acf_vida_util_restante'] if self['acf_vida_util_restante'] != 0
                else vida_util_restante,
                'acf_vida_util_residual': self['acf_vida_util_restante'] if self['acf_vida_util_restante'] != 0
                else vida_util_restante,
                'acf_valor_reposicion': self['acf_valor_reposicion'],
                'acf_valor_revalorizacion': self['acf_valor_revalorizacion'] if self[
                    'acf_valor_revalorizacion'] else None,
                'acf_factura_compra': self['acf_factura_compra'] if self["acf_factura_compra"] else None,
                'acf_fecha_compra': self['acf_fecha_compra'] if self["acf_fecha_compra"] else None,
                'custodio': self['custodio']['id'],
                'proveedor': self['proveedor']['id']
            }
            activo.write(val_activo)

        return asistente

    def button_draft(self):
        self.write({'state': 'borr'})

        for activo in self.activos_fijos:
            activo.write({'state': 'borr'})

    def last_day_of_month(self, date):
        if date.month == 12:
            return date.replace(day=31)
        return date.replace(month=date.month + 1, day=1) - timedelta(days=1)

    def validate(self):
        # Verifica si se encuentra en los dias permitidos para realizar los movimientos
        ir_default = self.env['ir.default'].sudo()
        dia_cierre = ir_default.get('res.config.settings', 'fecha_corte_operaciones')

        if int(dia_cierre) != 0:
            fecha_hoy = date.today()
            dia_fin_mes = self.last_day_of_month(fecha_hoy).strftime('%d')
            if int(dia_cierre) > int(dia_fin_mes):
                dia_cierre = dia_fin_mes
            fecha_actual = datetime.now()
            anio_mes_actual = fecha_actual.strftime('%Y-%m')
            fecha_actual_corte = anio_mes_actual + '-' + str(dia_cierre)
            fecha_actual_corte_date = datetime.strptime(fecha_actual_corte, '%Y-%m-%d').date()
            if fecha_hoy >= fecha_actual_corte_date:
                mensaje_error = 'No es posible realizar esta operacion, se encuentra fuera de la fecha limite para ' \
                                'movimientos: ' + str(fecha_actual_corte_date)
                raise AccessError(
                    mensaje_error)
            #

        self.state = 'val'
        json_seq_codes = {'111': 'saf-activos-fijos.seq_barcode_rec',
                          '112': 'saf-activos-fijos.seq_barcode_cue',
                          '113': 'saf-activos-fijos.seq_barcode_uio',
                          '114': 'saf-activos-fijos.seq_barcode_gye'}
        for activo in self.activos_fijos:
            codigo_centro_costo = activo.centro_costo_uno.codigo
            num_seq = json_seq_codes[codigo_centro_costo]
            seq_num_af = self.env['ir.sequence'].next_by_code(num_seq)
            code_taf = activo.taf_codigo.taf_codigo_contable

            while len(code_taf) < 3: code_taf = '0' + code_taf

            codigo_af = str(codigo_centro_costo) + str(code_taf) + seq_num_af
            searching = activo.name + ' [' + str(codigo_af) + ']'

            activo.write({
                'state': 'val',
                'acf_codigo_activo': codigo_af,
                'searching': searching,
                'active': True
            })
        return

    def unlink(self):
        for obj_af in self:
            if obj_af.state == 'val':
                raise UserError(
                    'No es posible eliminar activos que ya se encuentran en estado Validado...')
            return models.Model.unlink(self)

    def generar_codigo_barras(self):
        self.ensure_one()
        xml_id = 'saf-activos-fijos.report_barcode_af_masive'
        report_action = self.env.ref(xml_id).report_action(self)
        report_action.update({'close_on_report_download': True})
        return report_action
