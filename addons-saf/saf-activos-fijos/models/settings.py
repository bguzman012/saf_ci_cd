# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError


class ActivosFijosSetting(models.TransientModel):
    _inherit = 'res.config.settings'

    fecha_corte_operaciones = fields.Integer(
        string='Dia de corte para movimientos contables')
    diario_contable = fields.Many2one(
        "account.journal",
        string='Diario Contable Depreciación',
        required=True,
    )
    diario_contable_bajas = fields.Many2one(
        "account.journal",
        string='Diario Contable Bajas',
        required=True,
    )
    endpoint = fields.Char(
        string='Endpoint Servicio Web',
        default='http://deswf.ups.edu.ec:8080/v1/',
        required=True)
    endpoint_asientos = fields.Char(
        string='API Asientos contables',
        default='voucher/saveVouchers',
        required=True)

    def set_values(self):
        if self.fecha_corte_operaciones > 31 or self.fecha_corte_operaciones < 0:
            raise UserError(
                'El numero ingresado debe pertenecer a un dia del mes, es decir, debe ser menor a 31')

        super(ActivosFijosSetting, self).set_values()
        # self.env['ir.config_parameter'].set_param('saf-activos-fijos.fecha_corte_operaciones',
        #                                           self.fecha_corte_operaciones)
        # self.env['ir.config_parameter'].set_param('saf-activos-fijos.diario_contable',
        #                                           self.diario_contable.id)
        # self.env['ir.config_parameter'].set_param('saf-activos-fijos.diario_contable_bajas',
        #                                           self.diario_contable_bajas.id)
        self.env['ir.config_parameter'].set_param('saf-activos-fijos.endpoint',
                                                  self.endpoint)
        self.env['ir.config_parameter'].set_param('saf-activos-fijos.endpoint_asientos',
                                                  self.endpoint_asientos)

        ir_default = self.env['ir.default'].sudo()
        ir_default.set('res.config.settings', 'fecha_corte_operaciones', self.fecha_corte_operaciones)
        ir_default.set('res.config.settings', 'diario_contable', self.diario_contable.id)
        ir_default.set('res.config.settings', 'diario_contable_bajas', self.diario_contable_bajas.id)

    @api.model
    def get_values(self):
        res = super(ActivosFijosSetting, self).get_values()
        ICPS = self.env['ir.config_parameter'].sudo()

        endpoint = ICPS.get_param('saf-activos-fijos.endpoint')
        endpoint_asientos = ICPS.get_param('saf-activos-fijos.endpoint_asientos')

        ir_default = self.env['ir.default'].sudo()
        diario_deps = ir_default.get('res.config.settings', 'diario_contable')
        diario_bajas = ir_default.get('res.config.settings', 'diario_contable_bajas')
        fecha_corte = ir_default.get('res.config.settings', 'fecha_corte_operaciones')

        diario_contable_list = self.env['account.journal'].search(
            [('id', '=', diario_deps)])
        diario_contable_bajas_list = self.env['account.journal'].search(
            [('id', '=', diario_bajas)])

        if len(diario_contable_list) > 0:
            diario_contable_obj = diario_contable_list[0]
        else:
            diario_contable_obj = None

        if len(diario_contable_bajas_list) > 0:
            diario_contable_baja_obj = diario_contable_bajas_list[0]
        else:
            diario_contable_baja_obj = None

        res.update(
            fecha_corte_operaciones=fecha_corte,
            diario_contable=diario_contable_obj,
            diario_contable_bajas=diario_contable_baja_obj,
            endpoint=endpoint,
            endpoint_asientos=endpoint_asientos
        )
        return res
