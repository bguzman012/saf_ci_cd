# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CaracteristicaActivoFijo(models.Model):
    _name = 'saf.caracteristica_activo_fijo'
    _description = 'Caracteristica Actio Fijo'
    _rec_name = 'caf_valor_cadena_texto'

    acf_codigo = fields.Many2one(string='Activo Fijo',
                                 comodel_name='saf.activo_fijo',
                                 ondelete='cascade',
                                 required=False,
                                 index=True)
    asistente_af_codigo = fields.Many2one(string='Activo Fijo',
                                          comodel_name='saf.asistente_activo_fijo',
                                          ondelete='cascade',
                                          required=False,
                                          index=True)
    cta_codigo = fields.Many2one(
        'saf.caracteristica',
        string='Caracteristica',
        ondelete='cascade')
    lic_codigo = fields.Many2one(
        string='Lista Caracteristica',
        ondelete='cascade',
        comodel_name='saf.lista_caracteristica')
    caf_descripcion_texto = fields.Char(
        string='Descripcion Texto')
    caf_descripcion_numero = fields.Float(
        string='Descripcion numero',
        digits=(16, 2))
    caf_descripcion_fecha = fields.Date(
        string='Fecha')
    caf_tipo_cta_codigo = fields.Char(
        string='Tipo de caracteristica')
    caf_valor_cadena_texto = fields.Char(
        string='Valor',
        compute='_compute_cadena_valor',
        store=True)
    caf_creada_masivamente = fields.Boolean(
        'Creada masivamente',
        default=False)

    @api.depends('lic_codigo.name', 'caf_descripcion_texto', 'caf_descripcion_numero', 'caf_descripcion_fecha')
    def _compute_cadena_valor(self):
        for object in self:
            if object.caf_descripcion_texto:
                object.caf_valor_cadena_texto = object.caf_descripcion_texto
            if object.caf_descripcion_numero:
                object.caf_valor_cadena_texto = str(object.caf_descripcion_numero)
            if object.lic_codigo:
                object.caf_valor_cadena_texto = object.lic_codigo.name
            if object.caf_descripcion_fecha:
                object.caf_valor_cadena_texto = str(object.caf_descripcion_fecha)

    @api.onchange('acf_codigo', 'asistente_af_codigo')
    def _onchange_acf_codigo(self):
        if self.acf_codigo:
            carobj = self.env['saf.caracteristica_tipo_activo'].search(
                [('taf_codigo', '=', self.acf_codigo.taf_codigo.id)])

            self.acf_codigo = self.acf_codigo.ids[0]

        if self.asistente_af_codigo:
            carobj = self.env['saf.caracteristica_tipo_activo'].search(
                [('taf_codigo', '=', self.asistente_af_codigo.taf_codigo.id)])

            self.asistente_af_codigo = self.asistente_af_codigo.ids[0]

        carobj_list = []
        for data in carobj:
            carobj_list.append(data.car_codigo.id)
        res = {'domain': {'cta_codigo': [('id', 'in', carobj_list)]}}
        return res

    @api.onchange('cta_codigo')
    def _onchange_cta_codigo(self):
        # Si no ha escogido ningun tipo de caracteristica, envia una lista vacia
        if not self.cta_codigo.id:
            res = {'domain': {'lic_codigo': [('id', 'in', [])]}}
            return res

        # Anulamos todos los valores que no pertencescan al tipo de caracteristica
        self.caf_tipo_cta_codigo = self.cta_codigo.car_tipo
        if self.caf_tipo_cta_codigo == 'T':
            self.caf_descripcion_fecha = None
            self.lic_codigo = None
            self.caf_descripcion_numero = None
        if self.caf_tipo_cta_codigo == 'N':
            self.caf_descripcion_fecha = None
            self.lic_codigo = None
            self.caf_descripcion_texto = None
        if self.caf_tipo_cta_codigo == 'F':
            self.caf_descripcion_texto = None
            self.lic_codigo = None
            self.caf_descripcion_numero = None
        if self.caf_tipo_cta_codigo == 'S':
            self.caf_descripcion_fecha = None
            self.caf_descripcion_texto = None
            self.caf_descripcion_numero = None

        carobj = self.env['saf.lista_caracteristica'].search(
            [('car_codigo', '=', self.cta_codigo.id)])

        carobj_list = []
        for data in carobj:
            carobj_list.append(data.id)
        res = {'domain': {'lic_codigo': [('id', 'in', carobj_list)]}}
        return res


