# -*- coding: utf-8 -*-
from odoo import models, fields


class ListaCaracteristica(models.Model):
    _name = 'saf.lista_caracteristica'
    _description = 'Lista Caracteristica'
    _order = 'lic_orden asc'

    car_codigo = fields.Many2one(
        string='Caracteristica Codigo',
        comodel_name='saf.caracteristica',
        required=True)
    name = fields.Char(
        string='Nombre',
        required=True)
    taf_codigo = fields.Many2one(
        string='Tipo Activo Fijo',
        comodel_name='saf.tipo_activo_fijo')
    lic_descripcion_texto = fields.Char(
        string='Descripcion Texto')
    lic_orden = fields.Integer(
        string="Orden")
