from datetime import datetime, timedelta, date

import jwt
from odoo.fields import float_round

from odoo import http, models, api
import traceback
import json
from threading import Thread

from odoo.http import request


class ActivosApiREST(http.Controller):

    @http.route('/api/persistir_bienes', type='json', auth='jwt', methods=["POST"], csrf=False,
                website=False)
    def persistir_bienes(self, **kw):

        archi1 = open("datos.txt", "a")

        args_api = kw
        list_bienes = json.loads(args_api['bienes'])
        format_datetime = '%Y-%m-%d'
        json_seq_codes = {'111': 'saf-activos-fijos.seq_barcode_rec',
                          '112': 'saf-activos-fijos.seq_barcode_cue',
                          '113': 'saf-activos-fijos.seq_barcode_uio',
                          '114': 'saf-activos-fijos.seq_barcode_gye'}

        for bien in list_bienes:

            grupo_contable = request.env['saf.tipo_activo_fijo'].sudo().search(
                [('taf_codigo_contable', '=', bien[1])])
            centro_costo_uno = request.env['account.analytic.plan'].sudo().search(
                [('codigo', '=', bien[2])])
            centro_costo_dos = request.env['account.analytic.account'].sudo().search(
                [('code', '=', bien[10])])
            custodio = request.env['hr.employee'].sudo().search(
                [('work_contact_id.vat', '=', bien[20])], limit=1)

            if custodio.id is False:
                nombre_completo_custodio = bien[21].split(' ')
                if len(nombre_completo_custodio) == 3:
                    nombre_param = nombre_completo_custodio[2] + ' ' + nombre_completo_custodio[0] + ' ' \
                                   + nombre_completo_custodio[1]
                    custodio = request.env['hr.employee'].sudo().search(
                        [('name', '=', nombre_param)], limit=1)

                if len(nombre_completo_custodio) == 4:
                    nombre_param = nombre_completo_custodio[2] + ' ' + nombre_completo_custodio[3] + ' ' \
                                   + nombre_completo_custodio[0] + ' ' + nombre_completo_custodio[1]
                    custodio = request.env['hr.employee'].sudo().search(
                        [('name', '=', nombre_param)], limit=1)

            if custodio.id is False:

                existe_custodio = False

                # Si el activo pertenece a Zoila Tatiana Muñoz
                if bien[20] == '0300090073':
                    existe_custodio = True
                    cedula_custodio = '0104991195'

                # Si el activo pertenece a Enza Bosetti
                if bien[20] == 'PYA7335000':
                    cedula_custodio = '1757129836'
                    existe_custodio = True

                # Si a pesar de todas las busquedas posibles no existe, se le asigna al custodio
                # temporal por defecto para cada sede
                if existe_custodio is False:
                    cedula_custodio = centro_costo_uno.codigo

                custodio = request.env['hr.employee'].sudo().search(
                    [('work_contact_id.vat', '=', cedula_custodio)], limit=1)

            if custodio.id is False:
                activos_info = "No custodio;" + str(bien[0]) + ';' + str(bien[21]) + ';' + str(bien[19]) + ';' + str(
                    bien[20]) + ';' + str(bien[2]) + '\n'
                archi1.write(activos_info)
                continue

            codigo_centro_costo = centro_costo_uno.codigo
            num_seq = json_seq_codes[codigo_centro_costo]
            seq_num_af = request.env['ir.sequence'].next_by_code(num_seq)
            code_taf = grupo_contable.taf_codigo_contable

            while len(code_taf) < 3: code_taf = '0' + code_taf

            codigo_af = str(codigo_centro_costo) + str(code_taf) + seq_num_af
            cadena_searching = bien[3] + ' [' + str(codigo_af) + ']'

            vals_af_ins = {
                'centro_costo_uno': centro_costo_uno.id,
                'centro_costo_dos': centro_costo_dos.id if centro_costo_dos is not False else None,
                'name': bien[3],
                'searching': cadena_searching,
                'acf_codigo_activo': codigo_af,
                'taf_codigo': grupo_contable.id,
                'acf_fecha_alta': datetime.strptime(bien[4], format_datetime),
                'acf_fecha_inicio_depreciacion': datetime.strptime(bien[4], format_datetime),
                'acf_valor_alta': bien[5] if bien[5] > 0 else 0.01,
                'acf_vida_util_restante': bien[8],
                'acf_vida_util_residual': bien[17],
                'acf_valor_reposicion': bien[15],
                'custodio': custodio.id,
                'codigo_sigac': bien[0],
                'codigo_barras_sigac': bien[12],
                'bnaf_resumen': bien[18],
                'acf_fecha_compra': None
            }
            if bien[6] is not None:
                vals_af_ins['acf_fecha_baja'] = datetime.strptime(bien[6], format_datetime)
            # Si la vida util residual es 0, se ingresa como bien de control
            if bien[17] == 0:
                vals_af_ins['state'] = 'control'
            elif bien[6] is not None:
                vals_af_ins['state'] = 'baj'
            else:
                vals_af_ins['state'] = 'val'

            activo_creado = request.env['saf.activo_fijo'].sudo().create_af_migracion(vals_af_ins)

            # Si el activo tiene una marca asignada se crea un registro de caracteristica
            if bien[16] is not None:
                self.crear_caracteristica_af(grupo_contable=grupo_contable,
                                             nombre_caract=bien[16],
                                             activo=activo_creado,
                                             tipo_caract='Marca')

            # Si el activo tiene un modelo asignado se crea un registro de caracteristica
            if bien[14] is not None:
                self.crear_caracteristica_af(grupo_contable=grupo_contable,
                                             nombre_caract=bien[14],
                                             activo=activo_creado,
                                             tipo_caract='Modelo')

            # Si el activo tiene un numero de serie asignado se crear un registro en la caracteristica
            if bien[13] is not None:
                self.crear_caracteristica_num_serie(grupo_contable=grupo_contable,
                                                    val_texto=bien[13],
                                                    activo=activo_creado,
                                                    tipo_caract='serie')

            request.env.cr.commit()

        archi1.close()
        return {'RESULTADO': 'OK'}

    def crear_caracteristica_af(self, grupo_contable, nombre_caract, activo, tipo_caract):
        caracteristica_tipo_af = request.env['saf.caracteristica_tipo_activo'].sudo().search(
            [('taf_codigo', '=', grupo_contable.id),
             ('car_codigo.name', 'ilike', tipo_caract)])

        carcateristica = request.env['saf.lista_caracteristica'].sudo().search(
            [('car_codigo', '=', caracteristica_tipo_af.id),
             ('name', '=', nombre_caract)])

        val_caracteristica = {
            'cta_codigo': caracteristica_tipo_af.car_codigo.id,
            'lic_codigo': carcateristica.id,
            'acf_codigo': activo.id
        }

        request.env['saf.caracteristica_activo_fijo'].sudo(). \
            create(val_caracteristica)

    def crear_caracteristica_num_serie(self, grupo_contable, val_texto, activo, tipo_caract):
        caracteristica_tipo_af = request.env['saf.caracteristica_tipo_activo'].sudo().search(
            [('taf_codigo', '=', grupo_contable.id),
             ('car_codigo.name', 'ilike', tipo_caract)])

        val_caracteristica = {
            'cta_codigo': caracteristica_tipo_af.car_codigo.id,
            'caf_descripcion_texto': val_texto,
            'acf_codigo': activo.id
        }

        request.env['saf.caracteristica_activo_fijo'].sudo(). \
            create(val_caracteristica)

    @http.route('/api/persistir_grupos_contables', type='json', auth='jwt', methods=["POST"], csrf=False,
                website=False)
    def persistir_grupos_contables(self, **kw):
        args_api = kw
        list_tipos_af = args_api['tipos_af']
        for tipo_af in list_tipos_af:
            cu_activo = self.obtener_cuenta_por_codigo(tipo_af['cu_activo'])
            cu_depreciacion = self.obtener_cuenta_por_codigo(tipo_af['cu_depreciacion'])
            cu_dep_acum = self.obtener_cuenta_por_codigo(tipo_af['cu_dep_acum'])
            cu_baja = self.obtener_cuenta_por_codigo(tipo_af['cu_baja'])

            cuentas_lines = [(0, 0, {'tac_tipo_cuenta': 'ACTIVO',
                                     'cuc_codigo': cu_activo.id if cu_activo is not None else None}),
                             (0, 0, {'tac_tipo_cuenta': 'BAJA',
                                     'cuc_codigo': cu_baja.id if cu_baja is not None else None}),
                             (0, 0, {'tac_tipo_cuenta': 'DEPRECIACION',
                                     'cuc_codigo': cu_depreciacion.id if cu_depreciacion is not None else None}),
                             (0, 0, {'tac_tipo_cuenta': 'DEPRECIACION ACUMULADA',
                                     'cuc_codigo': cu_dep_acum.id if cu_dep_acum is not None else None})]

            tipo_af_persist = {
                'name': tipo_af['name'],
                'taf_vida_util': tipo_af['taf_vida_util'],
                'taf_codigo_contable': str(tipo_af['taf_codigo_contable']),
                'lista_car_codigo': cuentas_lines
            }
            request.env['saf.tipo_activo_fijo'].sudo().create(tipo_af_persist)
        return {'RESULTADO': 'OK'}

    def obtener_cuenta_por_codigo(self, codigo):
        if codigo is None:
            return None

        cuenta = request.env['account.account'].sudo().search(
            [('code', '=', codigo)])
        return cuenta

    @http.route('/api/persistir_cuentas', type='json', auth='jwt', methods=["POST"], csrf=False,
                website=False)
    def persistir_cuentas(self, **kw):
        args_api = kw
        list_cuentas = args_api['accounts']
        for cuenta in list_cuentas:
            request.env['account.account'].sudo().create(cuenta)
        return {'RESULTADO': 'OK'}

    @http.route('/api/persistir_caracteristica_num_serie', type='json', auth='jwt', methods=["POST"], csrf=False,
                website=False)
    def persistir_caracteristica_num_serie(self, **kw):
        # Crear caracteristicas y luego setear a cada tipo de activo fijo
        val_caracteristica = {
            'name': 'Numero de Serie',
            'car_tipo': 'T'
        }
        caracteristica = request.env['saf.caracteristica'].sudo().create(val_caracteristica)

        tipos_af = request.env['saf.tipo_activo_fijo'].sudo().search([])
        for taf in tipos_af:
            val_caracteristica_tipo_af = {
                'taf_codigo': taf.id,
                'car_codigo': caracteristica.id,
                'cta_obligatorio': 'S'
            }
            request.env['saf.caracteristica_tipo_activo'].sudo(). \
                create(val_caracteristica_tipo_af)

        return {'RESULTADO': 'OK'}

    @http.route('/api/persistir_all_caracteristicas', type='json', auth='jwt', methods=["POST"], csrf=False,
                website=False)
    def persistir_all_caracteristicas(self, **kw):
        args_api = kw
        list_caract = args_api['caracteristicas']
        tipo_caracteristica = args_api['tipo']
        json_caracteristicas = json.loads(list_caract)
        tipo_activo_init = json_caracteristicas[0][0]
        caracteristica = self.crear_caracteristicas(tipo_activo_init, tipo_caracteristica)

        cont = 0
        for caract in json_caracteristicas:

            if cont > 0:
                tipo_activo_actual = caract[0]
                if tipo_activo_init != tipo_activo_actual:
                    caracteristica = self.crear_caracteristicas(tipo_activo_actual, tipo_caracteristica)
                tipo_activo_init = caract[0]

            lista_caracteristica = {
                'car_codigo': caracteristica.id,
                'name': caract[1],
                'lic_descripcion_texto': caract[1],
            }
            request.env['saf.lista_caracteristica'].sudo(). \
                create(lista_caracteristica)

            cont += 1

        return {'RESULTADO': 'OK'}

    @http.route('/api/persistir_motivos_baja', type='json', auth='jwt', methods=["POST"], csrf=False,
                website=False)
    def persistir_motivos_baja(self, **kw):
        args_api = kw
        list_motivos_baja = args_api['list_motivos_baja']
        for motivo in list_motivos_baja:
            request.env['saf.motivos_baja'].sudo().create(motivo)
        return {'RESULTADO': 'OK'}

    def crear_caracteristicas(self, tipo_activo_init, tipo_caracteristica):
        tipo_af = request.env['saf.tipo_activo_fijo'].sudo().search(
            [('taf_codigo_contable', '=', tipo_activo_init)])

        val_caracteristica = {
            'name': tipo_caracteristica + ' ' + tipo_af.name,
            'car_tipo': 'S'
        }
        caracteristica = request.env['saf.caracteristica'].sudo().create(val_caracteristica)

        val_caracteristica_tipo_af = {
            'taf_codigo': tipo_af.id,
            'car_codigo': caracteristica.id,
            'cta_obligatorio': 'S'
        }
        request.env['saf.caracteristica_tipo_activo'].sudo(). \
            create(val_caracteristica_tipo_af)

        return caracteristica

    @http.route('/api/persistir_plan_analitic', type='json', auth='jwt', methods=["POST"], csrf=False,
                website=False)
    def persistir_plan_analiticas(self, **kw):
        args_api = kw
        list_planes = args_api['planes']
        for plan in list_planes:
            parent_contact = request.env['res.partner'].sudo().create({'name': plan['name']})
            plan['contacto'] = parent_contact.id
            request.env['account.analytic.plan'].sudo().create(plan)

        return {'RESULTADO': 'OK'}

    @http.route('/api/persistir_acc_analitic', type='json', auth='jwt', methods=["POST"], csrf=False,
                website=False)
    def persistir_acc_analiticas(self, **kw):
        args_api = kw
        self.delete_cuentas_analiticas()
        cuentas = args_api['cuentas']
        for cuenta in cuentas:
            request.env['account.analytic.account'].sudo().create(cuenta)
        return {'RESULTADO': 'OK'}

    @http.route('/api/persistir_empleado', type='json', auth='jwt', methods=["POST"], csrf=False,
                website=False)
    def persistir_empleado(self, **kw):
        args_api = kw
        list_empleados = args_api['empleados']

        list_ccs = [{
            'cc': '111',
            'name': 'RECTORADO'
        }, {
            'cc': '112',
            'name': 'SEDE CUENCA'
        }, {
            'cc': '113',
            'name': 'SEDE QUITO'
        }, {
            'cc': '114',
            'name': 'SEDE GUAYAQUIL'
        }]

        for emp_cc in list_ccs:
            obj_centro_costo = request.env['account.analytic.plan'].sudo().search([
                ('codigo', '=', str(emp_cc['cc']))])
            contacto = request.env['res.partner'].sudo().create({
                'name': emp_cc['name'],
                'vat': str(emp_cc['cc'])
            })
            request.env['hr.employee'].sudo().create(
                {'name': emp_cc['name'],
                 'work_contact_id': contacto.id,
                 'centro_costo': obj_centro_costo.id})

            request.env.cr.commit()

        for empleado in list_empleados:
            empleado_insert = empleado['employee']
            centros_costo = empleado['centro_costo']

            if centros_costo['uno'] is not None:
                obj_centro_costo = request.env['account.analytic.plan'].sudo().search([
                    ('codigo', '=', str(centros_costo['uno']))])
                if obj_centro_costo.id is False:
                    obj_centro_costo = request.env['account.analytic.plan'].sudo().search([
                        ('codigo', '=', '111')])
                empleado_insert['centro_costo'] = obj_centro_costo.id
            else:
                obj_centro_costo = request.env['account.analytic.plan'].sudo().search([
                    ('codigo', '=', '111')])
                empleado_insert['centro_costo'] = obj_centro_costo.id
            # if centros_costo['dos'] is not None:
            #     obj_centro_costo = request.env['account.analytic.plan'].sudo().search([
            #         ('codigo', '=', centros_costo['uno'])])
            #     empleado_insert['centro_costo'] = obj_centro_costo.id

            empleado_insert['codigo_persona'] = str(empleado['per_codigo']) if empleado['per_codigo'] is not None else None

            contacto = request.env['res.partner'].sudo().create(empleado['partner'])
            empleado_insert['work_contact_id'] = contacto.id
            empleado_inserted = request.env['hr.employee'].sudo().create(empleado_insert)
            request.env.cr.commit()

        return {'RESULTADO': 'OK'}

    def delete_cuentas_analiticas(self):
        cuentas = request.env['account.account'].sudo().search([])

        for cuenta in cuentas:
            try:
                cuenta.write({'deprecated': True})
            except Exception:
                pass

    @http.route('/api/persistir_lineas_dep', type='json', auth='jwt', methods=["POST"], csrf=False,
                website=False)
    def persistir_lineas_dep(self, **kw):
        args_api = kw
        list_lineas_dep = json.loads(args_api['lineas_dep'])
        format_datetime = '%Y-%m-%d'
        for linea in list_lineas_dep:
            activo = request.env['saf.activo_fijo'].sudo().search([('codigo_sigac', '=', linea[0])])
            if activo.id is False:
                continue

            vals = {
                'acf_codigo': activo.id,
                'mon_codigo': 2,
                'daf_periodo_depreciacion': datetime.strptime(linea[1], format_datetime),
                'daf_vida_util_residual': linea[2],
                'daf_valor_inicial_activo': activo.acf_valor_alta,
                'daf_depreciacion_mes': round(linea[4], 3),
                'daf_depreciacion_acum': round(linea[5], 3),
                'daf_valor_residual': round(linea[6], 3),
                'daf_status': 'DEP',
                'daf_dep_final': linea[8],
                'daf_dep_inicial': round(linea[7], 3),
                'daf_dep_acum_ini_anio': linea[10],
                'daf_vida_util_res_ini_anio': linea[11],
                'daf_mes_ini_anio': datetime.strptime(linea[12], format_datetime) if linea[12] is not None else None,
                'daf_vlr_corr_ini_anio': linea[9]
            }

            request.env['saf.depeciacion_activo_fijo'].create(vals)
            request.env.cr.commit()

        return {'status': 'ok'}

    @http.route('/api/persistir_bajas', type='json', auth='jwt', methods=["POST"], csrf=False,
                website=False)
    def persistir_bajas(self, **kw):
        args_api = kw
        list_bajas = json.loads(args_api['bajas'])
        format_datetime = '%Y-%m-%d'
        for baja in list_bajas:
            activo = request.env['saf.activo_fijo'].sudo().search([('codigo_sigac', '=', baja[0])])
            if activo.id is False:
                continue

            motivo_baja = request.env['saf.motivos_baja'].sudo().search([('code', '=', baja[2])])

            json_seq_codes = {'112': 'saf-activos-fijos.baja_cue',
                              '114': 'saf-activos-fijos.baja_gye',
                              '113': 'saf-activos-fijos.baja_uio',
                              '111': 'saf-activos-fijos.baja_rec'}
            codigo_centro_costo = activo.centro_costo_uno.codigo
            num_seq = json_seq_codes[codigo_centro_costo]

            vals = {
                'name': request.env['ir.sequence'].next_by_code(num_seq),
                'acf_lista_activos': [activo.id],
                'baf_fecha': datetime.strptime(baja[1], format_datetime),
                'state': 'val',
                'baf_custodio': activo.custodio.id,
                'sede': activo.centro_costo_uno.id,
                'motivo': motivo_baja.id,
                'baf_observaciones': 'Baja SIGAC'
            }

            request.env['saf.baja_activo_fijo'].create_migracion(vals)
            request.env.cr.commit()

        return {'status': 'ok'}

    @http.route('/api/generar_deps_faltantes', type='json', auth='jwt', methods=["POST"], csrf=False,
                website=False)
    def generar_deps_faltantes(self, **kw):

        # activos = request.env['saf.activo_fijo'].sudo(). \
        #     search([('acf_vida_util_residual', '!=', 0),
        #             ('acf_fecha_baja', '=', None),
        #             ('state', '=', 'val')])

        activos = request.env['saf.activo_fijo'].sudo(). \
            search([('acf_vida_util_residual', '!=', 0),
                    ('acf_fecha_baja', '=', None),
                    ('state', '=', 'val')])

        for activo in activos:
            dep_init = request.env['saf.depeciacion_activo_fijo'].sudo(). \
                search([('acf_codigo', '=', activo.id)],
                       order='daf_periodo_depreciacion desc')

            if len(dep_init) != 0:
                self.calcular_tablas_depreciaciones_faltantes(activo=activo,
                                                              ultima_depreciacion=dep_init[0])
            else:
                self.calcular_tabla_depreciacion(activo=activo,
                                                 fecha_inicio_dep=activo.acf_fecha_inicio_depreciacion,
                                                 verificar_dep_existentes=False)
            request.env.cr.commit()

        return {'status': 'ok'}

    def last_day_of_month(self, date):
        if date.month == 12:
            return date.replace(day=31)
        return date.replace(month=date.month + 1, day=1) - timedelta(days=1)

    def calcular_tabla_depreciacion(self, activo, fecha_inicio_dep, verificar_dep_existentes):
        moneda = request.env.company.currency_id.id
        moneda_precision_decimal = request.env.company.currency_id.decimal_places

        nperiodos = activo.acf_vida_util_restante
        v_inicial = activo.acf_valor_alta

        v_dep_final = 0
        v_util_residual = nperiodos
        v_util_ini_anio = nperiodos
        v_dep_acumulada = 0
        v_dep_acumulada_final = 0
        v_dep_mes = 0

        if fecha_inicio_dep.day > 14 and verificar_dep_existentes is False:
            # Si el dia es mayor a 14, se crea la primera depreciacion el siguiente mes
            if fecha_inicio_dep.day > 28:
                fecha_inicio_dep = fecha_inicio_dep.replace(day=28)
            carry, new_month = divmod(fecha_inicio_dep.month - 1 + 1, 12)
            new_month += 1
            fecha_inicio_dep = fecha_inicio_dep.replace(year=fecha_inicio_dep.year + carry, month=new_month)
            finmes = self.last_day_of_month(fecha_inicio_dep)
        else:
            finmes = self.last_day_of_month(fecha_inicio_dep)

        for periodo in range(nperiodos):
            if finmes.month == 1:
                v_dep_acumulada = 0
                v_dep_final = v_dep_acumulada_final
                v_util_ini_anio = v_util_residual
            else:
                v_dep_acumulada += v_dep_mes

            v_util_residual -= 1
            v_meses = v_util_ini_anio - v_util_residual
            dif_val_init_dep_fin = v_inicial - v_dep_final
            div_val_init_dep_fin_res_ini = dif_val_init_dep_fin / v_util_ini_anio
            mul_val_init_dep_fin_res_ini_meses = div_val_init_dep_fin_res_ini * v_meses
            v_dep_mes = mul_val_init_dep_fin_res_ini_meses - v_dep_acumulada
            v_dep_acumulada_final += v_dep_mes
            vals = {
                'acf_codigo': activo.id,
                'mon_codigo': moneda,
                'daf_periodo_depreciacion': finmes,
                'daf_vida_util_residual': v_util_residual,
                'daf_valor_inicial_activo': v_inicial,
                'daf_depreciacion_mes': float_round(v_dep_mes, precision_digits=moneda_precision_decimal),
                'daf_depreciacion_acum': float_round(v_dep_acumulada_final, precision_digits=moneda_precision_decimal),
                'daf_valor_residual': float_round(v_inicial - v_dep_acumulada_final,
                                                  precision_digits=moneda_precision_decimal),
                'daf_status': 'PEN'
            }

            finmes = self.last_day_of_month(finmes + timedelta(days=1))

            request.env['saf.depeciacion_activo_fijo'].create(vals)

    def calcular_tablas_depreciaciones_faltantes(self, activo, ultima_depreciacion):
        moneda = request.env.company.currency_id.id
        moneda_precision_decimal = request.env.company.currency_id.decimal_places

        nperiodos = ultima_depreciacion.daf_vida_util_residual
        v_inicial = activo.acf_valor_alta

        v_dep_final = ultima_depreciacion.daf_dep_final

        v_util_residual = nperiodos
        v_util_ini_anio = ultima_depreciacion.daf_vida_util_res_ini_anio

        v_dep_acumulada_final = ultima_depreciacion.daf_depreciacion_acum
        v_dep_mes = ultima_depreciacion.daf_depreciacion_mes

        finmes = self.last_day_of_month(ultima_depreciacion.daf_periodo_depreciacion + timedelta(days=1))

        # Calcular las depreciaciones a partir de las depreciaciones migradas del SIGAC
        v_dep_acumulada = 0
        if finmes.month != 1:

            # Calcula el inicio del año para obtener la depreciacion acumulada por anio
            inicio_anio = datetime(finmes.year, 1, 1)
            fin_mes_inicio_anio = self.last_day_of_month(inicio_anio)

            meses_sum = request.env['saf.depeciacion_activo_fijo'].sudo(). \
                search([('acf_codigo', '=', activo.id),
                        ('daf_periodo_depreciacion', '>=', fin_mes_inicio_anio),
                        ('daf_periodo_depreciacion', '<', finmes)], order="daf_periodo_depreciacion asc")

            v_dep_tmp = 0
            for val_mes in meses_sum:

                if val_mes.daf_periodo_depreciacion.month != 1:
                    v_dep_acumulada += v_dep_tmp

                v_dep_tmp = val_mes.daf_depreciacion_mes

        for periodo in range(nperiodos):
            if finmes.month == 1:
                v_dep_acumulada = 0
                v_dep_final = v_dep_acumulada_final
                v_util_ini_anio = v_util_residual
            else:
                v_dep_acumulada += v_dep_mes

            v_util_residual -= 1
            v_meses = v_util_ini_anio - v_util_residual
            dif_val_init_dep_fin = v_inicial - v_dep_final
            div_val_init_dep_fin_res_ini = dif_val_init_dep_fin / v_util_ini_anio
            mul_val_init_dep_fin_res_ini_meses = div_val_init_dep_fin_res_ini * v_meses
            v_dep_mes = mul_val_init_dep_fin_res_ini_meses - v_dep_acumulada
            v_dep_acumulada_final += v_dep_mes

            vals = {
                'acf_codigo': activo.id,
                'mon_codigo': moneda,
                'daf_periodo_depreciacion': finmes,
                'daf_vida_util_residual': v_util_residual,
                'daf_valor_inicial_activo': v_inicial,
                'daf_depreciacion_mes': float_round(v_dep_mes, precision_digits=moneda_precision_decimal),
                'daf_depreciacion_acum': float_round(v_dep_acumulada_final, precision_digits=moneda_precision_decimal),
                'daf_valor_residual': float_round(v_inicial - v_dep_acumulada_final,
                                                  precision_digits=moneda_precision_decimal),
                'daf_status': 'PEN'
            }

            finmes = self.last_day_of_month(finmes + timedelta(days=1))

            request.env['saf.depeciacion_activo_fijo'].create(vals)

    @http.route('/api/update_end_month_deps', type='json', auth='jwt', methods=["POST"], csrf=False,
                website=False)
    def update_end_month_deps(self, **kw):
        request.env.cr.execute("""
                update saf_depeciacion_activo_fijo 
                set daf_periodo_depreciacion = (date_trunc('month', daf_periodo_depreciacion) + interval '1 month - 1 day')
                where daf_status = 'DEP'
            """)
        request.env.cr.commit()
        return {'status': 'ok'}
