import xlsxwriter

from odoo import models, fields, api
import json

import io
import xlwt
from datetime import datetime, timedelta
import base64
from odoo.exceptions import ValidationError
import itertools
import random
from datetime import datetime, timedelta, date


class AltasAFReportWizard(models.TransientModel):
    _name = "saf.alta_af_grupo_report"
    _description = "Wizard para obtener reporte de altas de activo fijo por grupo contable"

    company = fields.Many2one(string='Empresa',
                              required=True,
                              comodel_name='res.company',
                              default=lambda self: self.env.company)
    centro_costo_uno = fields.Many2many(
        "account.analytic.plan",
        string="Centro de Costo 1",
        domain=lambda self: self._get_allowed_centros_costo()
    )
    centro_costo_dos = fields.Many2many(
        "account.analytic.account",
        string="Centro de Costo 2"
    )
    taf_codigo = fields.Many2many(string='Grupo',
                                  comodel_name='saf.tipo_activo_fijo')
    fecha_desde = fields.Date(string="Fecha desde",
                              required=True,
                              default=fields.Date.today())
    fecha_hasta = fields.Date(string="Fecha hasta",
                              required=True,
                              default=lambda self: self.last_day_of_month())
    excel_file = fields.Binary('Descarga el reporte',
                               filename='filename',
                               readonly=True)
    filename = fields.Char('Excel File')
    activos_existen = fields.Char(
        string='Existen activos')

    @api.constrains('fecha_desde', 'fecha_hasta')
    def check_fecha_hasta_mayor(self):
        for rec in self:
            if rec.fecha_desde >= rec.fecha_hasta:
                raise ValidationError('Fecha hasta no puede ser mayor o igual a la fecha desde')

    def last_day_of_month(self):
        fecha_hoy = date.today()
        if fecha_hoy.month == 12:
            return fecha_hoy.replace(day=31)
        return fecha_hoy.replace(month=fecha_hoy.month + 1, day=1) - timedelta(days=1)

    def _get_allowed_centros_costo(self):
        allowed_centros_costo = self.env.user.centros_costo.ids
        return [('id', 'in', allowed_centros_costo)]

    def export_data(self):
        grupos_contables = self.armar_data_activos()

        if len(grupos_contables) == 0:
            self.activos_existen = 'NO'
            return {
                'name': 'Altas de activos fijos por grupo contable',
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'views': [(False, 'form')],
                'res_model': 'saf.alta_af_grupo_report',
                'res_id': self.id,
                'target': 'new',
            }
        else:
            self.activos_existen = 'SI'

        headers = ['#', 'Empr.', 'Código', 'Descripcion', 'Fecha alta', 'Vida Útil Inicial',
                   'V.U.R', 'Valor Inicial', 'Dep. Acum.', 'Valor Residual']

        style_header_main = xlwt.easyxf('font: name Times New Roman, color-index black, bold on,'
                                        'height 280', num_format_str='  # ,##0.00')
        style_header = xlwt.easyxf('font: name Times New Roman, color-index black, bold on',
                                   num_format_str='  # ,##0.00')
        style_text = xlwt.easyxf('font: name Times New Roman, color-index black')
        style_number = xlwt.easyxf('font: name Times New Roman, color-index black', num_format_str='  ##0.00')
        style_date = xlwt.easyxf('font: name Times New Roman, color-index black', num_format_str='DD-MMM-YYYY')

        wb = xlwt.Workbook()

        for grupo in grupos_contables:
            num_diff = random.randint(0, 500)
            ws = wb.add_sheet(str(grupo['centro_1_nombre'].split('-')[1][0:2]) + '-' +
                              str(grupo['centro_2_nombre'].split('-')[1][0:2]) + '-' +
                              str(grupo['taf_nombre'][0:2] + '-' + str(num_diff)))
            ws.col(0).width = 4200
            ws.col(2).width = 7000
            ws.col(3).width = 7000
            ws.col(4).width = 7000

            periodo = str(self.fecha_desde) + ' ~ ' + str(self.fecha_hasta)

            # Encabezado
            ws.write(0, 3, ' Costos de Activos por Grupos Contables al periodo %s '
                     % periodo, style_header_main)

            ws.write(1, 0, ' GRUPO CONTABLE', style_header)
            ws.write(1, 1, grupo['taf_nombre'])

            ws.write(2, 0, ' CENTRO COSTO 1', style_header)
            ws.write(2, 1, grupo['centro_1_nombre'])

            ws.write(2, 3, ' CENTRO COSTO 2', style_header)
            ws.write(2, 4, grupo['centro_2_nombre'])

            colh = 0
            for head in headers:
                ws.write(4, colh, head, style_header)
                colh += 1

            colh = 0
            row = 5
            suma_val_inicial = 0
            suma_val_depr_acum = 0
            for activo in grupo['activos']:
                ws.write(row, colh, row - 4, style_text)
                ws.write(row, colh + 1, activo['company_id'], style_text)
                ws.write(row, colh + 2, activo['codigo_activo'], style_text)
                ws.write(row, colh + 3, activo['name'], style_number)
                ws.write(row, colh + 4, activo['acf_fecha_alta'], style_date)
                ws.write(row, colh + 5, activo['vu_inicial'], style_number)
                ws.write(row, colh + 6, activo['vur'], style_number)
                ws.write(row, colh + 7, activo['acf_valor_alta'], style_number)
                ws.write(row, colh + 8, activo['depr_acum'], style_number)
                ws.write(row, colh + 9, activo['acf_valor_alta'] - activo['depr_acum'], style_number)
                suma_val_inicial = suma_val_inicial + activo['acf_valor_alta']
                suma_val_depr_acum = suma_val_depr_acum + activo['depr_acum']
                row += 1

            ws.write(row + 1, 6, 'TOTAL', style_header)
            ws.write(row + 1, 7, suma_val_inicial, style_number)
            ws.write(row + 1, 8, suma_val_depr_acum, style_number)

        fp = io.BytesIO()

        wb.save(fp)

        filename = "Altas por grupos contables {}.xls".format(datetime.strftime(fields.Date.today(), '%Y-%m-%d'),
                                                              style_header)

        self.excel_file = base64.encodebytes(fp.getvalue())
        self.filename = filename
        fp.close()
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'saf.alta_af_grupo_report',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': self.id,
            'target': 'new',
        }

    def obtener_depreciacion_acum(self, activo):
        lista_depreciaciones = self.env['saf.depeciacion_activo_fijo'].search(
            [('acf_codigo', '=', activo.id),
             ('daf_status', '=', 'DEP')],
            order='id desc',
            limit=1)

        valor_depreciacion = lista_depreciaciones[0].daf_depreciacion_acum if len(lista_depreciaciones) > 0 else 0

        return valor_depreciacion

    def armar_data_activos(self):

        # Si no selecciona niguno, toma todos los grupos contables existentes
        if not self.taf_codigo.ids:
            lista_taf = self.env['saf.tipo_activo_fijo'].search([])
            lista_taf_ids = [taf.id for taf in lista_taf]
        else:
            lista_taf_ids = self.taf_codigo.ids

        # Si no selecciona niguno, toma todos los centro de costo dos
        if not self.centro_costo_dos.ids:
            lista_centros_dos = self.env['account.analytic.account'].search([])
            lista_centros_dos_ids = [taf.id for taf in lista_centros_dos]
        else:
            lista_centros_dos_ids = self.centro_costo_dos.ids

        # Si no selecciona niguno, toma todos los centro de costo uno
        if not self.centro_costo_uno.ids:
            lista_centros_uno = self.env['account.analytic.plan'].search([])
            lista_centros_uno_ids = [taf.id for taf in lista_centros_uno]
        else:
            lista_centros_uno_ids = self.centro_costo_uno.ids

        activos = self.env['saf.activo_fijo'].search(
            [('acf_fecha_alta', '>=', self.fecha_desde),
             ('acf_fecha_alta', '<=', self.fecha_hasta),
             ('taf_codigo', 'in', lista_taf_ids),
             ('centro_costo_dos', 'in', lista_centros_dos_ids),
             ('centro_costo_uno', 'in', lista_centros_uno_ids),
             ('state', '=', 'val'),
             ('company_id', '=', self.company.id)],
            order='centro_costo_uno desc, centro_costo_dos desc, taf_codigo  desc')

        lista_activos_agrupados = []
        obj_centros_taf = {}
        cont = 0
        for activo in activos:
            if cont == 0:
                obj_centros_taf = {
                    'centro_1': activo.centro_costo_uno.id,
                    'centro_2': activo.centro_costo_dos.id,
                    'taf': activo.taf_codigo.id,
                    'centro_1_nombre': str(activo.centro_costo_uno.codigo) + '-' + str(activo.centro_costo_uno.name),
                    'centro_2_nombre': str(activo.centro_costo_dos.code) + '-' + str(activo.centro_costo_dos.name),
                    'taf_nombre': activo.taf_codigo.name,
                    'activos': []
                }

            obj_tmp_compare = {
                'centro_1': activo.centro_costo_uno.id,
                'centro_2': activo.centro_costo_dos.id,
                'taf': activo.taf_codigo.id,
                'centro_1_nombre': str(activo.centro_costo_uno.codigo) + '-' + str(activo.centro_costo_uno.name),
                'centro_2_nombre': str(activo.centro_costo_dos.code) + '-' + str(activo.centro_costo_dos.name),
                'taf_nombre': activo.taf_codigo.name,
                'activos': []
            }

            if obj_centros_taf['centro_1'] == obj_tmp_compare['centro_1'] and obj_centros_taf['centro_2'] == \
                    obj_tmp_compare['centro_2'] and obj_centros_taf['taf'] == obj_tmp_compare['taf']:
                valor_acum = self.obtener_depreciacion_acum(activo)
                activo_json = {
                    'company_id': activo.company_id.id,
                    'taf_codigo': activo.taf_codigo.name,
                    'name': activo.name,
                    'dependencia': activo.centro_costo_dos.name,
                    'acf_fecha_alta': activo.acf_fecha_alta,
                    'acf_valor_alta': activo.acf_valor_alta,
                    'depr_acum': valor_acum,
                    'vu_inicial': activo.acf_vida_util_restante,
                    'vur': activo.acf_vida_util_residual,
                    'codigo_activo': activo.acf_codigo_activo
                }
                obj_centros_taf['activos'].append(activo_json)
            else:
                lista_activos_agrupados.append(obj_centros_taf)
                obj_centros_taf = obj_tmp_compare
                valor_acum = self.obtener_depreciacion_acum(activo)
                activo_json = {
                    'company_id': activo.company_id.id,
                    'taf_codigo': activo.taf_codigo.name,
                    'name': activo.name,
                    'dependencia': activo.centro_costo_dos.name,
                    'acf_fecha_alta': activo.acf_fecha_alta,
                    'acf_valor_alta': activo.acf_valor_alta,
                    'vu_inicial': activo.acf_vida_util_restante,
                    'vur': activo.acf_vida_util_residual,
                    'depr_acum': valor_acum,
                    'codigo_activo': activo.acf_codigo_activo
                }
                obj_centros_taf['activos'].append(activo_json)

            if len(activos) - 1 == cont:
                lista_activos_agrupados.append(obj_centros_taf)

            cont += 1

        return lista_activos_agrupados

    def action_print_report(self):
        fecha_desde = str(self.fecha_desde)
        fecha_hasta = str(self.fecha_hasta)
        company = self.company.name

        activos = self.armar_data_activos()

        if len(activos) == 0:
            self.activos_existen = 'NO'

            return {
                'name': 'Altas de activos fijos por grupo contable',
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'views': [(False, 'form')],
                'res_model': 'saf.alta_af_grupo_report',
                'res_id': self.id,
                'target': 'new',
            }
        else:
            self.activos_existen = 'SI'

        data = {
            'periodo': fecha_desde + ' ~ ' + fecha_hasta,
            'activos': activos,
            'empresa': company,
            'form': self.read()[0]
        }

        return self.env.ref('saf-activos-fijos.altas_af_grupo_report_pdf').with_context(
            landscape=True).report_action(
            self,
            data=data)
