from odoo import models, fields, api


class ActivoReportWizard(models.TransientModel):
    _name = "activo.report.wizard"
    _description = "Imprimir Activos Reportes"

    taf_codigo = fields.Many2many(string='Tipo Activo Fijo', comodel_name='saf.tipo_activo_fijo')
    fecha_desde = fields.Date(string="Fecha Inicio", required=True)
    fecha_hasta = fields.Date(string="Fecha Fin", required=True)
    company = fields.Many2one(string='Compañia',  required=True, comodel_name='res.company')

    def get_activos(self, estado):
        nombre_activos = ''
        if len(self.taf_codigo.ids) != 0:

            lista_tipo_activos = self.taf_codigo.ids
            lista_string_ids_activos = '('
            cont = 0
            for obj_elem in lista_tipo_activos:
                if cont == 0:
                    lista_string_ids_activos = lista_string_ids_activos + str(obj_elem)
                else:
                    lista_string_ids_activos = lista_string_ids_activos + ',' + str(obj_elem)
                cont = cont + 1

            lista_string_ids_activos = lista_string_ids_activos + ')'

            sentencia = 'select name from saf_tipo_activo_fijo where id in ' + lista_string_ids_activos

            self._cr.execute(sentencia)
            lista_tipo = self._cr.dictfetchall()

            cont_tipos = 0
            for tipo_activo in lista_tipo:
                if cont_tipos == 0:
                    nombre_activos = tipo_activo['name']
                else:
                    nombre_activos = nombre_activos + ', ' + tipo_activo['name']

                cont_tipos = cont_tipos + 1

        else:

            nombre_activos = 'Todos'

        sentencia_select = ''
        sentencia_from = ' from saf_activo_fijo saf ' \
                         'JOIN saf_tipo_activo_fijo stf ON saf.taf_codigo = stf.id '
        sentencia_where = ''
        sentencia_order_by = ''

        if estado == 'A':

            sentencia_select = "select saf.acf_fecha_alta, saf.acf_valor_alta, " \
                               "split_part(saf.acf_fecha_alta::TEXT, ' ', 1) as fecha," \
                               "saf.taf_codigo, saf.name, saf.acf_codigo_activo, " \
                               "stf.name as name_tipo_activo_fijo "

            sentencia_where = " where saf.acf_fecha_alta >= '" + str(self.fecha_desde) + \
                              "' AND saf.acf_fecha_alta <= '" + str(self.fecha_hasta) + "'" + \
                              " AND saf.estado = 'ACTIVO'"

            sentencia_order_by = " ORDER BY saf.taf_codigo, saf.acf_fecha_alta, saf.name"


        else:

            sentencia_select = "select saf.acf_fecha_baja, saf.acf_valor_baja, " \
                               "split_part(saf.acf_fecha_baja::TEXT, ' ', 1) as fecha_baja_spl," \
                               "saf.taf_codigo, saf.name, saf.acf_codigo_activo, " \
                               "stf.name as name_tipo_activo_fijo "

            sentencia_where = " where saf.acf_fecha_baja >= '" + str(self.fecha_desde) + \
                              "' AND saf.acf_fecha_baja <= '" + str(self.fecha_hasta) + "'" + \
                              " AND saf.estado = 'INACTIVO'"

            sentencia_order_by = " ORDER BY saf.taf_codigo, saf.acf_fecha_baja, saf.name "

        if len(self.taf_codigo.ids) != 0:
            sentencia_where = sentencia_where + ' AND saf.taf_codigo in ' + lista_string_ids_activos

        sentencia_final = sentencia_select + sentencia_from + sentencia_where + sentencia_order_by

        self._cr.execute(sentencia_final)
        lista_activos = self._cr.dictfetchall()

        data_return = {
            'lista_activos': lista_activos,
            'nombre_tipo_activos': nombre_activos
        }
        return data_return

    def action_print_report(self):
        periodo = str(self.fecha_desde) + ' - ' + str(self.fecha_hasta)
        company = self.company.name

        dict_activos = self.get_activos('A')
        dict_activos_inactivos = self.get_activos('I')

        lista_activos = dict_activos['lista_activos']
        nombre_activos = dict_activos['nombre_tipo_activos']
        lista_activos_inactivos = dict_activos_inactivos['lista_activos']

        data = {
            'tipos_activos': nombre_activos,
            'periodo': periodo,
            'lista_activos': lista_activos,
            'lista_activos_inactivos': lista_activos_inactivos,
            'empresa': company,
            'form': self.read()[0]
        }
        return self.env.ref('saf-activos-fijos.action_report_activos_pdf').with_context(landscape=True).report_action(self,
                                                                                                                   data=data)
