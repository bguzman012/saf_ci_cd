# -*- coding: utf-8 -*-
{
    'name': "Formatos Plantillas EMAIL SAF",
    'summary': """
        Modulo que contiene las plantillas de los formatos de reportes""",
    'description': """
        Modulo que contiene las plantillas de los formatos de reportes
    """,
    'author': "UPS",
    'website': "https://www.ups.edu.ec/",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': ['base', 'account', 'mail', 'saf-activos-fijos'],
    'data': [
        'templates/user-created-template.xml',
        'templates/depreciacion-template.xml',
        'data/servidor_email.xml'

    ]
}
