import http.client
import logging

import jwt

from odoo import models
from odoo.exceptions import AccessDenied
from odoo.http import request

_logger = logging.getLogger(__name__)


class IrHttp(models.AbstractModel):
    _inherit = "ir.http"

    # Se agrega un metodo de seguridad para el apirest JWT
    @classmethod
    def _auth_method_jwt(cls):
        token = request.httprequest.headers.get('Authorization')

        try:
            _, token = token.split(' ')
        except ValueError:
            raise http.client.UNAUTHORIZED

        try:
            data = jwt.decode(token, 'secret_key', algorithms=['HS256'])
        except:
            raise http.client.UNAUTHORIZED

        request.update_env(user=data['user_id'])
