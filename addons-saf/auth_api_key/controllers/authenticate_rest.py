from odoo import http, models, api
from datetime import datetime, timedelta, date
import jwt

from odoo.http import Response
from odoo.http import request


class AuthenticateREST(http.Controller):

    @http.route('/api/authenticate', auth='none', type='json', methods=['POST'], csrf=False)
    def authenticate(self, **kw):

        name = kw.get('name')
        password = kw.get('password')
        db = kw.get('db')

        if not name or not password or not db:
            return {'error': 'Datos inválidos'}

        try:

            user_auth = request.session.authenticate(db, name, password)

        except:
            return {'error': 'Usuario o contraseña inválidos'}

        if not user_auth:
            return {'error': 'Usuario o contraseña inválidos'}

        # Generate a new JWT
        exp_date = datetime.utcnow() + timedelta(minutes=30)
        token = jwt.encode({'user_id': user_auth, 'exp': exp_date}, 'secret_key',
                           algorithm='HS256')

        return {'token': token}
