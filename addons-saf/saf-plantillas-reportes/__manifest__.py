# -*- coding: utf-8 -*-
{
    'name': "Formatos Plantillas SAF",
    'summary': """
        Modulo que contiene las plantillas de los formatos de reportes""",
    'description': """
        Modulo que contiene las plantillas de los formatos de reportes
    """,
    'author': "UPS",
    'website': "https://www.ups.edu.ec/",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': ['base', 'account', 'mail'],
    'data': [
        'templates/general_a4_ups.xml',
        'templates/general_horizontal_a4_ups.xml',

        'formatos/formato_a4.xml',
        'formatos/formato_a4_header_customize.xml',
        'formatos/formato_a4_resumen_depreciaciones.xml',
        'formatos/formato_a4_resumen_depreciaciones_vertical.xml'
        # 'reports/reporte_cuadre_cierre_af.xml',
    ]
}
