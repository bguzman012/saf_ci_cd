from odoo import models


class SingUpInherit(models.Model):
    _inherit = "res.company"

    def init(self):
        for company in self.search([]):
            paperformat = self.env.ref('saf-plantillas-reportes.format_a4_ups', False)
            if paperformat:
                company.write({'paperformat_id': paperformat.id})
        sup = super(SingUpInherit, self)
        if hasattr(sup, 'init'):
            sup.init()
