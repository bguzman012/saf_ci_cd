import cx_Oracle
import requests
import json
from threading import Thread
from time import sleep, perf_counter

def persistir_tipos_activo(con, url):
    cursor = con.cursor()
    sql_tipos_activos = """select GRCN_CDG, GRCN_DSC, CNTS_CDG_CTBLE_ACT, CNTS_CDG_CTBLE_DEP, 
                        GRCN_VIDA_UTIL, CNTS_CDG_CTBLE_DEP_COM, CNTS_CDG_CTBLE_BAJA from grcn"""

    cursor.execute(sql_tipos_activos)
    
    rows = cursor.fetchall()
    list_tipos_af = []
    for row in rows:
        taf = {
            'taf_codigo_contable': row[0],
            'name': row[1],
            'taf_vida_util': row[4],
            'cu_activo': row[2],
            'cu_depreciacion': row[3],
            'cu_dep_acum': row[5],
            'cu_baja': row[6] 
        }
        list_tipos_af.append(taf)
        
    json_list_taf = {
         "params": {
             'tipos_af': list_tipos_af
             }} 
    reponse = requests.post(url, json=json_list_taf)
   
    
def persistir_motivos_baja(con, url):
    cursor = con.cursor()
    sql_motivos_baja = """select * from sigac.view_tipo_baja"""

    cursor.execute(sql_motivos_baja)
    
    rows = cursor.fetchall()
    list_motivos = []
    for row in rows:
        motivo_baja = {
            'name': row[1],
            'code': row[0] 
        }
        list_motivos.append(motivo_baja)
        
    json_list_motivos_baja = {
         "params": {
             'list_motivos_baja': list_motivos
             }} 
    reponse = requests.post(url, json=json_list_motivos_baja)


def persistir_employee(con, url):
    cursor = con.cursor()
    
    gth_estado_civil = {
        1: 'married',
        2: 'divorced',
        3: 'single',
        4: 'widower',
        5: 'cohabitant'
    }
    
    sql_centros_costo = """select ICC_NIVEL_1, ICC_NIVEL_2 from gth.gth_contrato gtc LEFT JOIN
                            gth.gth_int_centro_costo gtcc ON gtc.ams_codigo = gtcc.ams_codigo where gtc.con_estado = 
                            'A' AND per_codigo = :per_codigo"""
    
    sql_employees = """select ESC_CODIGO, PER_APELLIDOS, PER_NOMBRES, PER_GENERO, PER_FECHA_NACIMIENTO, 
                PER_CALLE_PRINCIPAL, PER_NRO_CASA, PER_CALLE_SECUNDARIA, PER_REFERENCIA, TII_CODIGO, 
                PER_NRO_IDENTIFICACION, PER_CODIGO from gth.gth_persona where PER_ELIMINADO = 'N' """

    cursor.execute(sql_employees)
    rows = cursor.fetchall()
    list_employees = []
    for row in rows:
        cursor_centros_costo = con.cursor()
        cursor_centros_costo.execute(sql_centros_costo, per_codigo=row[11])
        row_ccosto = cursor_centros_costo.fetchone()
        
        obj_employee = {
            'centro_costo': {
                'uno': row_ccosto[0] if row_ccosto is not None else None,
                'dos': row_ccosto[1] if row_ccosto is not None else None,   
            },
            'partner': {
                'name': row[2] + ' ' + row[1],
                'street': row[5],
                'street2': row[7],
                'l10n_latam_identification_type_id': 1 if row[9] == 1 else 2,
                'vat': row[10]
            },
            'employee':{
                'name': row[2] + ' ' + row[1],
                'marital': gth_estado_civil[row[0]]
            }}
        list_employees.append(obj_employee)
        
    json_list_empleados = {
        "params": {
            'empleados': list_employees
            }} 
    reponse = requests.post(url, json=json_list_empleados)
    

def persistir_centros_costo(con, url):
    cursor = con.cursor()
    
    sql_centros_costo = "SELECT LEVEL, nctb_cdg_centro_costo, nctb_dsc descripcion FROM NCTB WHERE EMPR_CDG = " \
                                "1 AND NCTB_ACTIVO = 'S' CONNECT BY PRIOR NCTB_CDG_NIVEL = NCTB_CDG_NIVEL_PADRE  AND " \
                                "NCTB_CDG = 1 START WITH NCTB_NIVEL_CTAS = 1 AND NCTB_CDG = 1"

    cursor.execute(sql_centros_costo)
    
    rows = cursor.fetchall()
    list_planes = []
    for row in rows:
        plan = {
            'name': row[2],
            'codigo': row[1] 
        }
        list_planes.append(plan)
        
    json_list_planes = {
        "params": {
            'planes': list_planes
            }} 
    reponse = requests.post(url, json=json_list_planes)
    

def persistir_centros_costo_dos(con, url):
    cursor = con.cursor()
    
    sql_centros_costo_dos = "SELECT NCTB_CDG_NIVEL, NCTB_DSC FROM sigac.niveles_contabilizacion n WHERE nctb_cdg = 2 and level = 3 START WITH " \
        "nctb_cdg_nivel = 'R2' connect by prior nctb_cdg_nivel = nctb_cdg_nivel_padre"

    cursor.execute(sql_centros_costo_dos)
    
    rows = cursor.fetchall()
    list_cu_analytic = []
    for row in rows:
        acc_analytic = {
            'name': row[1],
            'code': row[0] 
        }
        list_cu_analytic.append(acc_analytic)
        
    json_list_acc_analytics = {
        "params": {
            'cuentas': list_cu_analytic
            }} 
    reponse = requests.post(url, json=json_list_acc_analytics)


def persistir_cuenta_contable(con, url):
    cursor = con.cursor()
    
    tipos_codigos_contab = {
        '1': 'asset_current',
        '2': 'liability_current',
        '3': 'equity',
        '4': 'income',
        '5': 'expense',
        '7': 'off_balance',
        '8': 'off_balance'
    }

    cursor.execute('SELECT * FROM sigac.cuentas')
    
    rows = cursor.fetchall()
    list_accounts = []
    for row in rows:
        obj_cuenta = {
            'name':row[1],
            'code': row[0],
            'company_id': 1
            }
        es_banco = row[6]
        if es_banco == 'S':
            obj_cuenta['account_type'] = 'asset_cash'
            list_accounts.append(obj_cuenta)
            continue
        
        letra_init = row[0][0]
        obj_cuenta['account_type'] = tipos_codigos_contab[letra_init]
        list_accounts.append(obj_cuenta)
    
    json_list_accounts = {
        "params": {
            'accounts': list_accounts
            }} 
    reponse = requests.post(url, json=json_list_accounts)


def persistir_caracteristica_modelo(con, url):
    cursor = con.cursor()

    cursor.execute('select distinct GRCN_CDG, bnaf_modelo from sigac.bienes_activo_fijo where bnaf_modelo is not null and empr_cdg = 1 order by grcn_cdg')
    
    rows = cursor.fetchall()
    list_caract_modelos = json.dumps(rows) 
    json_list_caract_modelos = {
        "params": {
            'caracteristicas': list_caract_modelos,
            'tipo': 'MODELO'
            }} 
    reponse = requests.post(url, json=json_list_caract_modelos)
    

def persistir_caracteristica_marca(con, url):
    cursor = con.cursor()

    cursor.execute('select distinct GRCN_CDG, bnaf_marca from sigac.bienes_activo_fijo where bnaf_marca is not null and empr_cdg = 1 order by grcn_cdg')
    
    rows = cursor.fetchall()
    list_caract_marcas = json.dumps(rows) 
    json_list_caract_marcas = {
        "params": {
            'caracteristicas': list_caract_marcas,
            'tipo': 'MARCA'
            }} 
    reponse = requests.post(url, json=json_list_caract_marcas)
    
    
def persistir_lineas_dep(url, offset, next_num):
    con = cx_Oracle.connect(user='BGUZMAN', password='BGUZMAN_2022', dsn='dbdes.ups.edu.ec:1521/upsd.ups.edu.ec')
    cursor = con.cursor()
    
    cursor.execute("""select BNAF_CRR, TO_CHAR(CMAF_ULT_PER, 'YYYY-MM-DD'), CMAF_UTIL_RESIDUAL, CMAF_VLR_INICIAL_BIEN, CMAF_DEP_MES, CMAF_DEP_ACUM_FINAL, CMAF_VLR_RESIDUAL
                    from sigac.correccion_monet_activo_fijo ldep where empr_cdg = 1 and CMAF_DEP_MES != 0 order by bnaf_crr, cmaf_ult_per
                    OFFSET :offset ROWS FETCH NEXT :next ROWS ONLY""", [offset, next_num])
    
    rows = cursor.fetchall()
    print(rows[0], '  ROW ONE')
    list_lineas_dep = json.dumps(rows) 
    json_list_lineas_dep = {
        "params": {
            'lineas_dep': list_lineas_dep
            }} 
    reponse = requests.post(url, json=json_list_lineas_dep)
    
    
    
def persistir_caracteristica_num_serie(url):
    json_list_caract_num_serie = {
        "params": {
            'tipo': 'NUM_SERIE'
            }} 
    reponse = requests.post(url, json=json_list_caract_num_serie)


def prepare_data_thread_bienes(con, url):
    cursor_count = con.cursor()
    cursor_count.execute("""select count(*) from sigac.bienes_activo_fijo where empr_cdg = 1 order by bnaf_crr""")
    count_deps = cursor_count.fetchone()
    num_hilos = 16
    num_lotes = int(count_deps[0]/num_hilos)
    total_num_lotes = num_lotes * num_hilos
    dif_restante = count_deps[0] - total_num_lotes
    threads = []
    offset = 0
    
    start_time = perf_counter()
    
    x = range(num_hilos)
    for n in x:
        if n == (num_hilos-1):
            num_lotes = num_lotes + dif_restante
        t = Thread(target=persistir_bienes, args=(url, offset, num_lotes))
        threads.append(t)
        t.start()
        offset += num_lotes
        
    for t in threads:
        t.join()
        
    end_time = perf_counter()
    print(f'Ha tomado {end_time- start_time: 0.2f} segundo(s) para completar.')
    
    
def prepare_data_thread_lineas_dep(con, url):
    cursor_count = con.cursor()
    cursor_count.execute("""select count(*) from sigac.correccion_monet_activo_fijo ldep where empr_cdg = 1 
                         and CMAF_DEP_MES != 0 order by bnaf_crr, cmaf_ult_per""")
    count_deps = cursor_count.fetchone()
    num_hilos = 16
    num_lotes = int(count_deps[0]/num_hilos)
    total_num_lotes = num_lotes * num_hilos
    dif_restante = count_deps[0] - total_num_lotes
    threads = []
    offset = 0
    
    start_time = perf_counter()
    
    x = range(num_hilos)
    for n in x:
        if n == (num_hilos-1):
            num_lotes = num_lotes + dif_restante
       
        t = Thread(target=persistir_lineas_dep, args=(url, offset, num_lotes))
        threads.append(t)
        t.start()
        offset += num_lotes
        
    for t in threads:
        t.join()
        
    end_time = perf_counter()
    print(f'Ha tomado depreciaciones {end_time- start_time: 0.2f} segundo(s) para completar.')

    
def persistir_bienes(url, offset, next_num):
    con = cx_Oracle.connect(user='BGUZMAN', password='BGUZMAN_2022', dsn='dbdes.ups.edu.ec:1521/upsd.ups.edu.ec')
    
    cursor = con.cursor()

    cursor.execute("""select baf.BNAF_CRR, baf.GRCN_CDG, baf.DPND_CDG, baf.BNAF_DSC,  TO_CHAR(baf.BNAF_FCH_ALTA, 'YYYY-MM-DD'), baf.BNAF_VLR_ALTA,  
                TO_CHAR(baf.BNAF_FCH_BAJA, 'YYYY-MM-DD'),baf.TPBJ_CDG, baf.BNAF_VIDA_UTIL, baf.EDLB_CDG, baf.CNCS_CDG, baf.BNAF_RESP_RUT, 
                baf.BNAF_CODIGO_BARRAS, baf.BNAF_NUM_SERIE, baf.BNAF_MODELO, baf.BNAF_VLR_REPOSICION, baf.BNAF_MARCA, baf.BNAF_UTIL_RESIDUAL, 
                baf.BNAF_RESUMEN, caf.PRSH_CDG_PERS, caf.CUBI_CEDULA, baf.BNAF_RESP_NMB
                from sigac.bienes_activo_fijo baf join custodio_activo_fijo caf on baf.BNAF_CRR = caf.BNAF_CRR where empr_cdg = 1
                OFFSET :offset ROWS FETCH NEXT :next ROWS ONLY""", [offset, next_num])
    
    rows = cursor.fetchall()
    print(rows[0])
    
    list_activos = json.dumps(rows)
    json_list_caract_marcas = {
        "params": {
            'bienes': list_activos
            }} 
    reponse = requests.post(url, json=json_list_caract_marcas)
    

def persistir_bajas(con, url):
    cursor = con.cursor()

    cursor.execute("""select BNAF_CRR, TO_CHAR(BBAF_FCH_BAJA, 'YYYY-MM-DD'), BBAF_CAUSA from 
                   sigac.bienes_baja_activo_fijo where empr_cdg = 1""")
    
    rows = cursor.fetchall()
    list_bajas = json.dumps(rows) 
    json_list_bajas = {
        "params": {
            'bajas': list_bajas
            }} 
    reponse = requests.post(url, json=json_list_bajas)
    
def prepare_data_dep_faltantes(url):
    json_list_params = {
        "params": {}} 
    reponse = requests.post(url, json=json_list_params)


def update_end_month_deps(url):
    json_list_params = {
        "params": {}} 
    reponse = requests.post(url, json=json_list_params)


def main():
        # Inicializa la conexion con la BD
        
    url_main = 'http://localhost:8069/' 
    url_cuentas_contables = url_main + 'api/persistir_cuentas'
    url_centro_costo_uno =  url_main + 'api/persistir_plan_analitic'
    url_centro_costo_dos =  url_main + 'api/persistir_acc_analitic'
    url_employees = url_main + 'api/persistir_empleado'
    url_grupos_contable = url_main + 'api/persistir_grupos_contables'
    url_motivos_baja = url_main + 'api/persistir_motivos_baja'
    url_caracteristicas = url_main + 'api/persistir_all_caracteristicas'
    url_bienes = url_main + 'api/persistir_bienes'
    url_num_serie = url_main + 'api/persistir_caracteristica_num_serie'
    url_lineas_dep = url_main + 'api/persistir_lineas_dep'
    url_update_end_month_deps = url_main + 'api/update_end_month_deps'
    url_lineas_dep_faltantes = url_main + 'api/generar_deps_faltantes'
    url_persistir_bajas = url_main + 'api/persistir_bajas'
    
    cx_Oracle.init_oracle_client(lib_dir=r'C:\Users\BryamWilsonGuzmanCab\Downloads\instantclient-basic-windows.x64-21.7.0.0.0dbru\instantclient_21_7')
    con = cx_Oracle.connect(user='BGUZMAN', password='BGUZMAN_2022', dsn='dbdes.ups.edu.ec:1521/upsd.ups.edu.ec')
    
    persistir_centros_costo(con, url_centro_costo_uno)
    print('######### CENTROS COSTO 100 % ########')
    persistir_centros_costo_dos(con, url_centro_costo_dos)
    print('######### CENTROS COSTO DOS 100 % ########')
    persistir_employee(con, url_employees)
    print('######### EMPLOYEES 100 % ########')
    persistir_cuenta_contable(con, url_cuentas_contables)
    print('######### CUENTA CONTABLE 100 % ########')
    persistir_tipos_activo(con, url_grupos_contable)
    print('######### TIPO ACTIVO 100 % ########')
    persistir_motivos_baja(con, url_motivos_baja)
    print('######### MOTIVOS BAJA 100 % ########')
    persistir_caracteristica_modelo(con, url_caracteristicas)
    print('######### CARACTERISTICA MODELO 100 % ########')
    persistir_caracteristica_marca(con, url_caracteristicas)
    print('######### CARACTERISTICA MARCA 100 % ########')
    persistir_caracteristica_num_serie(url_num_serie)
    print('######### CARACTERISTICA NUM_SERIE 100 % ########')
    prepare_data_thread_bienes(con, url_bienes)
    print('######### BIENES 100 % ########')
    prepare_data_thread_lineas_dep(con, url_lineas_dep)
    print('######### DEPRECIACIONES 100 % ########')
    update_end_month_deps(url_update_end_month_deps)
    print('######### UPDATE_DEPS_END_MONTH 100 % ########')
    prepare_data_dep_faltantes(url_lineas_dep_faltantes)
    print('######### DEPS_FALTANTES 100 % ########')
    persistir_bajas(con, url_persistir_bajas)
    print('######### BAJAS 100 % ########')


    con.close()


if __name__ == "__main__":
    main()
