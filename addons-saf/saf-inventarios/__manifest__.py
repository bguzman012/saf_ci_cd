# -*- coding: utf-8 -*-
{
    'name': "Inventarios UPS",
    'summary': """
        Modulo de inventarios""",
    'description': """
        Módulo de gestión de inventarios de la UPS
    """,
    'author': "UPS",
    'website': "https://www.ups.edu.ec/",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': ['base', 'hr'],
    'data': [
        'views/product_category_inherit.xml'
    ]
}
