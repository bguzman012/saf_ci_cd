from datetime import datetime, timedelta, date
from odoo import http, models, api
import json


class ActivosApiREST(http.Controller):

    @http.route('/api/persistir_categorias', type='json', auth='jwt', methods=["POST"], csrf=False,
                website=False)
    def persistir_categorias(self, **kw):
        args_api = kw
        list_cates_lg = json.loads(args_api['cates_lg'])
        list_cates_fa = json.loads(args_api['cates_fa'])
        list_cates_sf = json.loads(args_api['cates_sf'])
        for categoria_lg in list_cates_lg:
            cate_insert = {
                'name': categoria_lg[1],
                'code': categoria_lg[0]
            }
            http.request.env['product.category'].sudo().create(cate_insert)

        for categoria_fa in list_cates_fa:
            cate_lg_padre = http.request.env['product.category'].sudo().search(
                [('code', '=', categoria_fa[2])])

            cate_insert = {
                'name': categoria_fa[1],
                'code': categoria_fa[0],
                'parent_id': cate_lg_padre.id
            }
            http.request.env['product.category'].sudo().create(cate_insert)

        for categoria_sf in list_cates_sf:
            cate_fa_padre = http.request.env['product.category'].sudo().search(
                [('code', '=', categoria_sf[2])])

            cate_insert = {
                'name': categoria_sf[1],
                'code': categoria_sf[0],
                'parent_id': cate_fa_padre.id
            }
            http.request.env['product.category'].sudo().create(cate_insert)

        return {'RESULTADO': 'OK'}

    @http.route('/api/persistir_materiales', type='json', auth='jwt', methods=["POST"], csrf=False,
                website=False)
    def persistir_materiales(self, **kw):
        args_api = kw
        list_prods = json.loads(args_api['prods'])
        try:
            for prod in list_prods:
                category = http.request.env['product.category'].sudo().search(
                    [('code', '=', prod[1])], limit=1)

                if category.id is False:
                    category = http.request.env['product.category'].sudo().search(
                        [('name', '=', 'All')], limit=1)

                produ_insert = {
                    'name': prod[2],
                    'detailed_type': 'product',
                    'categ_id': category.id,
                    'list_price': prod[6]
                }
                http.request.env['product.template'].sudo().create(produ_insert)

            return {'RESULTADO': 'OK'}
        except Exception as e:
            s = str(e)
            return {'ERROR': s}
