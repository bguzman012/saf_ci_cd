from odoo import models, fields, api


class ProductCategoryInherit(models.Model):
    _inherit = "product.category"

    code = fields.Char(string='Código')
